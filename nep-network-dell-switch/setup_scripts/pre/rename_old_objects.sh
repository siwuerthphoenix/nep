. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

declare -A command_objects
command_objects["nx-c-centreon_dell_n4000"]="nx-c-centreon-dell-n4000"
command_objects["nx-c-centreon_dell_os10_snmp"]="nx-c-centreon-dell-os10-snmp"

echo "Removing legacy Director Objects"

## Rename command objects
for c in "${!command_objects[@]}"; do
    tmp=$(icingacli director command exist "$c")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director command set "$c" --object_name "${command_objects[$c]}"
    fi
done
