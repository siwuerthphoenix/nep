#!/bin/bash
## Install Plugin for Centeon

dnf -y install https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/p/perl-URI-Encode-1.1.1-11.el8.noarch.rpm
dnf -y --enablerepo=centreon-*-stable* install centreon-plugin-Network-Dell-N4000
dnf -y --enablerepo=centreon-*-stable* install centreon-plugin-Network-Dell-Os10-Snmp.noarch
