{
    "volume_group": "vg00",
    "ip_pre" : "192.168.1",
    "Services": [
        {
            "name": "nep",
            "ip_post": null,
            "drbd_minor": 208,
            "drbd_port": 7995,
            "folder": "/neteye/shared/nep/",
            "service":null,
            "collocation_resource": "cluster_ip",
            "size": "1024"
        }
    ]
}