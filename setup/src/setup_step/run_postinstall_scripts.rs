use crate::*;

impl Context {
    pub fn run_postinstall_scripts(&mut self, _package: &Package) -> Result<()> {
        log::info!("Step 6: Running post-install scripts");
        let post = self.build_path(&["setup_scripts", "post"])?;
        let random_folder = self.exec_env.random_folder();
        self.exec_env.exec_all("mkdir", &["-p", random_folder.to_str().unwrap()], None)?;
        self.exec_env.sync_files_all(&post, random_folder.to_str().unwrap())?;
        for file in sorted_in_folder(&random_folder)? {
            self.exec_env.exec_all("sh", &[file.to_str().unwrap()], None)?;
        }
        Ok(())
    }
}