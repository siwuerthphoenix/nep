use crate::*;

impl Context {
    pub fn run_preinstall_scripts(&mut self, _package: &Package) -> Result<()> {
        // step 2 run preinstall scripts
        log::info!("Step 2: Running pre-install scripts");
        let pre = self.build_path(&["setup_scripts", "pre"])?;
        let random_folder = self.exec_env.random_folder();
        self.exec_env.exec_all("mkdir", &["-p", random_folder.to_str().unwrap()], None)?;
        self.exec_env.sync_files_all(&pre, random_folder.to_str().unwrap())?;
        // assume that the scripts are in the same path on all nodes
        for file in sorted_in_folder(&random_folder)? {
            self.exec_env.exec_all("sh", &[file.to_str().unwrap()], None)?;
        }
        Ok(())
    }
}