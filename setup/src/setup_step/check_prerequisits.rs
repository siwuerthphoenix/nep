use crate::*;
use semver::Version;
use regex::Regex;
use lazy_static::lazy_static;
use std::collections::BTreeSet;

/// List of the names of the packages that are not under NEP control, but can
/// be installed through Neteye
const NETEYE_MANAGEMNT_MODULES: &[&str] = &[   
    "neteye",
    "neteye-logmanagement",
    "neteye-siem",
    "neteye-vmd",
    "neteye-slm",
    "neteye-asset",
    "neteye-ntopng",
    "neteye-cmd"
];

impl Context {
    pub fn check_prerequisits(&mut self, package: &Package) -> Result<()> {
        log::info!("Step 1: Checking prerequisites");

        log::info!("Step 1.1: Checking Packages prerequisites");
        for (pkg_dep_name, dep_version) in &package.packages_deps {
            match self.installed_packages.get(pkg_dep_name) {
                Some(installed_version) => {
                    if !dep_version.matches(installed_version) {
                        bail!(
                            "The package '{}' is required with version '{}' but the latest installed one is '{}'.",
                            pkg_dep_name, dep_version, installed_version,
                        );
                    }
                }
                None => {
                    let _ = self.packages.search_package_by_name(&pkg_dep_name);
                    bail!(
                        "The package '{}' is required with version '{}'. Please install it.",
                        pkg_dep_name, dep_version,
                    );
                }
            }
        }

        log::info!("Step 1.2: Checking Neteye prerequisites");
        if let Some(required_neteye_version) = &package.neteye_version {
            let version = std::env::var("YUM0").or_else(|_| std::env::var("DNF0"))?;
            log::trace!("Got raw neteye version: '{}'", &version);
            // they added sprint releases so instead of XX.YY now we also have
            // XX.YY-srZZ but we are interested only in the first two parts
            let version = version.split('-').next().unwrap();
            let neteye_version = Version::parse(&format!("{}.0", version))?;
            log::trace!("Got neteye version: '{}'", &neteye_version);
            if !required_neteye_version.matches(&neteye_version) {
                bail!(
                    "Needed Neteye Version '{}' but found '{}'.",
                    required_neteye_version, neteye_version,
                );
            }
        }

        log::info!("Step 1.3: Checking Neteye modules prerequisites");
        if !package.neteye_modules.is_empty() {
            let result = self.exec_env.exec_master("yum", &[
                "groups", "list", "installed", "ids", "--enablerepo=neteye", "neteye*",
            ], None)?;

            lazy_static! {
                static ref RE: Regex = Regex::new(r"NetEye.+\((.+)\)").unwrap();
            }

            let mut installed_modules = BTreeSet::new();
            for line in result.stdout.lines() {
                for cap in RE.captures_iter(line) {
                    log::trace!("Found installed neteye module: '{}'", &cap[1]);
                    installed_modules.insert(cap[1].to_string());
                }
            }

            for module in &package.neteye_modules {
                if !NETEYE_MANAGEMNT_MODULES.contains(&module.as_str()) {
                    log::warn!("Warning: Neteye module '{}' is not under nep-setup management", &module);
                }
                if !installed_modules.contains(module.as_str()) {
                    bail!("Neteye module '{}' is not installed", module);
                }
                log::trace!("Neteye module '{}' is satisfied", module);
            }
        }

        Ok(())
    }
}