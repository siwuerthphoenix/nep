use crate::*;

impl Context {
    pub fn copy_files(&mut self, _package: &Package) -> Result<()> {
        // step 3, copy all files
        log::info!("Step 3: Copying files");

        let neteye_local_root = self.build_path(&["custom_files", "neteye_local_root"])?;
        let system_root =  self.build_path(&["custom_files", "system_root"])?;

        // copy files rsync -a --exclude='*.md' {source} {target}
        log::info!("Step 3.1: Copying files: neteye_local_root");
        self.exec_env.sync_files_all(&neteye_local_root, "/neteye/local/")?;
        log::info!("Step 3.2: Copying files: system_root");
        self.exec_env.sync_files_all(&system_root, "/")?;

        log::info!("Step 3.3: Copying files: fs_packages");
        for fs_package in &self.fs_packages {
            let fs_package_path =  self.build_path(
                &["custom_files", "neteye_shared_root", &fs_package.name]
            )?;
            
            if !fs_package_path.exists() {
                continue;
            }

            log::info!("Found fs_package {}", fs_package.name);
            log::info!("Copying files for fs_package {}", fs_package.name);
            // change permissions of the subfolders
            log::info!("Changing permissions files for fs_package {}", fs_package.name);
            for sub in sorted_in_folder(&fs_package_path)? {

                // 20230909 WP-PERC: Don't remember why owner of a directory named conf must be root:root.
                //                   Removing this code.

                // let ownership = if sub.file_name().unwrap().to_string_lossy() == "conf" {
                //     "root:root".into()
                // } else {
                //     format!("{}:{}", fs_package.user, fs_package.group)
                // };
                let ownership = format!("{}:{}", fs_package.user, fs_package.group);

                self.exec_env.exec_master("chown", &["-R", &ownership, sub.to_str().unwrap()], None)?;
            }

            // copy the folder
            if self.confs.is_cluster() {
                self.exec_env.sync_files_endpoint(&fs_package.endpoint, &fs_package_path, &fs_package.path.to_string_lossy())?;
            } else {
                self.exec_env.sync_files_master(&fs_package_path, &fs_package.path.to_string_lossy())?;
            }
        }
        Ok(())
    }
}