use std::path::PathBuf;
use std::str::FromStr;
use super::*;
use crate::utils::{uname_n, ismount};
use anyhow::{Result, anyhow, Context, bail};
use std::collections::{BTreeSet, BTreeMap};
use semver::Version;

#[derive(Debug, Clone, Hash)]
/// Collection of **all** the paths we will use
pub struct Confs {
    /// The path where neteye writes the other nodes informations on a cluster
    pub neteye_cluster_file: PathBuf,
    /// Nep from RPM with the packages that can be installed to update to a 
    /// newer version
    pub stage: PathBuf,
    /// drbd mount, shared between nodes, ignore plz, only used to check 
    /// which node is the master one
    pub packages_root: PathBuf,
    /// Historical folder where each package has subfolder with all the 
    /// versions that were found, with v.{version}
    pub packages: PathBuf,
    /// Path to the fs packages
    pub fs_packages_file: PathBuf,
    /// Working dir, this is either stage or the last versioned package
    /// depending on installation or reinstallation
    pub working_dir: PathBuf,
}

impl std::default::Default for Confs {
    fn default() -> Self {
        Confs {
            neteye_cluster_file: PathBuf::from_str("/etc/neteye-cluster").unwrap(),
            stage:               PathBuf::from_str("/usr/share/neteye/nep/").unwrap(),
            working_dir:         PathBuf::from_str("/usr/share/neteye/nep/").unwrap(),
            packages_root:       PathBuf::from_str("/neteye/shared/nep/").unwrap(),
            packages:            PathBuf::from_str("/neteye/shared/nep/data/packages/").unwrap(),
            fs_packages_file:    PathBuf::from_str("/usr/share/neteye/nep/setup/conf/fs_packages.json").unwrap(),
        }
    }
}

impl Confs {
    /// Create a new confs from the base 
    pub fn new(stage: PathBuf, packages: PathBuf, neteye_cluster_file: PathBuf, fs_packages_file: PathBuf) -> Result<Self> {
        if !stage.exists() {
            bail!("The stage folder doesn't exists: {:?}", stage);
        }
        if !packages.exists() {
            bail!("The packages folder doesn't exists: {:?}", packages);
        }
        //if !neteye_cluster_file.exists() {
        //    bail!("The neteye_cluster_file folder doesn't exists: {:?}", neteye_cluster_file);
        //}
        if !fs_packages_file.exists() {
            bail!("The fs_packages_file folder doesn't exists: {:?}", fs_packages_file);
        }

        Ok(Confs{
            working_dir: stage.clone(),
            stage,
            packages,
            neteye_cluster_file,
            fs_packages_file,
            ..Default::default()
        })
    }

    pub fn get_packages(&self, installed_packages: &BTreeMap<String, Version>) -> Result<Packages> {
        let mut result = Packages::default();

        for (name, obj) in get_packages_in_path(&self.stage) {
            match obj {
                Ok(package) => {
                    if let Some(installed_version) = installed_packages.get(&name) {
                        match installed_version.cmp(&package.version) {
                            std::cmp::Ordering::Greater | std::cmp::Ordering::Equal => {
                                if result.installed.insert(name.clone(), package).is_some() {
                                    bail!("Duplicated install package {:?}", name);
                                }
                            },
                            std::cmp::Ordering::Less => {
                                if result.updatable.insert(name.clone(), package).is_some() {
                                    bail!("Duplicated updatable package {:?}", name);
                                }
                            },
                        }
                    } else {
                        if result.available.insert(name.clone(), package).is_some() {
                            bail!("Duplicated available package {:?}", name);
                        }
                    }
                },
                Err(e) => {
                    if result.errored.insert(name.clone(), e).is_some() {
                        bail!("Duplicated errored package {:?}", name);
                    }
                },
            }
        }

        Ok(result)
    }

    /// Identify if the current machine is a cluster
    /// On netye systems the file [`NETEYE_CLUSTER_FILE`] contains the ips of the 
    /// other machines in the cluster.
    #[must_use]
    pub fn is_cluster(&self) -> bool {
        self.neteye_cluster_file.is_file()
    }

    #[inline(always)]
    #[must_use]
    /// Check that we are on the master node by checking that packages_root is
    /// a mounted disk
    pub fn is_nep_master(&self) -> Result<bool> {
        ismount(&self.packages_root)
    }

    /// either run `uname -n` or read the `Hostname` field from [`self.neteye_cluster_file`]
    #[must_use]
    pub fn get_neteye_hostname(&self) -> Result<String> {
        return uname_n();
        
        /* 
        TODO: check this.
            on the python script threre was this logic, but in which case is it
            actially called? nep should be executed on the master, so it will
            always just be the uname_n. Is this right?

        if !self.is_cluster() {
            return uname_n();
        }

        let data = std::fs::read_to_string(&self.neteye_cluster_file)
            .with_context(|| format!("Cannot read the {} file", self.neteye_cluster_file.to_string_lossy()))?;

        // parse the json
        let v: serde_json::Value = serde_json::from_str(&data)
            .with_context(||"The neteye-cluster file is not a valid json file")?;
        let obj = v.as_object()
            .ok_or(anyhow!("The neteye-cluster file is a valid json but it's not an object!"))?;
        
        // obj["Hostname"]
        let hostname = obj.get("Hostname")
            .ok_or(anyhow!("Hostname field not present"))?;
        let hostname = hostname.as_str()
            .ok_or(anyhow!("The Hostname field is not a string"))?;

        Ok(hostname.trim().to_string())
        */
    }

    /// either run `uname -n` or read the `Nodes` field from [`self.neteye_cluster_file`]
    #[must_use]
    pub fn get_neteye_nodes(&self) -> Result<BTreeSet<String>> {
        let mut res = BTreeSet::new();
        if !self.is_cluster() {
            //bail!("You can't ask for the neteye nodes on a non-cluster node.");
            return Ok(BTreeSet::new());
        }
        let data = std::fs::read_to_string(&self.neteye_cluster_file)
            .with_context(|| format!("Cannot read the {} file", self.neteye_cluster_file.to_string_lossy()))?;

        log::trace!("The content of '{}' is: {}", self.neteye_cluster_file.to_str().unwrap(), &data);

        // parse the json
        let v: serde_json::Value = serde_json::from_str(&data)
            .with_context(||"The neteye-cluster file is not a valid json file")?;
        let obj = v.as_object()
            .ok_or(anyhow!("The neteye-cluster file is a valid json but it's not an object!"))?;

        // obj["Nodes"]
        let nodes = obj.get("Nodes")
            .ok_or(anyhow!("Nodes field not present"))?;
        let nodes = nodes.as_array()
            .ok_or(anyhow!("The Nodes field is not an array"))?;

        // extract the string fields
        for node in nodes.iter() {
            let node = node.as_object().unwrap();

            match node.get("hostname_ext") {
                Some(hostname_ext) => {
                    let hostname_ext = hostname_ext.as_str().unwrap();
                    if !res.insert(hostname_ext.to_string()) {
                        log::warn!("The node hostname '{}' is present multiple times!", hostname_ext);
                    }
                },
                None => {
                    bail!(
                        "The following cluster node does not contains the required 'hostname_ext' field: {}",
                        serde_json::to_string_pretty(node)?,
                    );
                }
            }
        }
        log::trace!("Found nodes hostnames '{:?}' ", &res);
        Ok(res)
    }
}
