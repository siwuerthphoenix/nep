#! /bin/bash

# Old packages shihpped configuration file containing personal info.
# If this file has not been changed, it will be removev without asking.

FILE_TO_REMOVE=/etc/httpd/conf.d/httpd-proxypass-alyvix.conf

if [ -f ${FILE_TO_REMOVE} ]; then
    MD5SUM_REF='14986d3c764279079325548ee72ff3dc'
    MD5SUM=$(md5sum ${FILE_TO_REMOVE} | awk '{ print $1 }')

    if [ "${MD5SUM_REF}" = ${MD5SUM} ]; then
        echo rm -f ${FILE_TO_REMOVE}
    fi
fi