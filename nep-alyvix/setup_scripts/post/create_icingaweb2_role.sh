#! /bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh

# Create the Icingaweb2 Role that allows Alyvix Monitoring Plugin to get Testcase data
# If a user uses this role to log into NetEye, it will not see any test case, while the
# Plugin is still able to get all the required data to work.

ICINGAWEB2_DRBD_RESOURCE_NAME="icingaweb2_drbd_fs"

if is_cluster && ! is_active "$ICINGAWEB2_DRBD_RESOURCE_NAME" ; then
    echo "[i] Inactive Cluster Node, skipping"
    return 0
fi

ICINGAWEB2_ROLES_FILE="/neteye/shared/icingaweb2/conf/roles.ini"
ROLE_NAME="alyvix-service-check"
ROLE_DEFINITION="
\n\
[${ROLE_NAME}]\n\
users = \"alyvix-check\"\n\
permissions = \"module/alyvix,alyvix/*,module/neteye\"\n\
name = \"alyvix-service-check\""


if ! grep "\[${ROLE_NAME}\]" "${ICINGAWEB2_ROLES_FILE}" ; then
    echo "  [i] Adding role '${ROLE_NAME}' to $ICINGAWEB2_ROLES_FILE"
    if echo -e "$ROLE_DEFINITION" >> "${ICINGAWEB2_ROLES_FILE}" ; then
        echo "    [i] Role '${ROLE_NAME}' correctly added to $ICINGAWEB2_ROLES_FILE"
    else
        echo "    [-] Error while adding role '${ROLE_NAME}' to $ICINGAWEB2_ROLES_FILE"
        exit 1
    fi
fi
