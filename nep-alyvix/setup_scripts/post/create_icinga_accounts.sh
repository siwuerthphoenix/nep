#! /bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh

SERVICE="icinga2-master"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

echo '[i] Creating Icinga2 API user account "alyvix-check"'
icinga_pwd_file="/root/.pwd_icinga2_alyvix_check"
if [ ! -f "${icinga_pwd_file}" ]; then
    echo '[!] Unable to get account password from pwd file'
    exit 1
fi

api_user_file=/neteye/shared/icinga2/conf/icinga2/conf.d/nx-alyvix-service-users.conf
icinga_pwd="$(cat "${icinga_pwd_file}")"
if [ -f "${api_user_file}" ]; then
    echo '[i] User account should already exist. Skipping.'
else
    cat << EOF > "${api_user_file}"
/**
* Used by Alyvix Service Check plugin to identify and set status of the proper (passive) services
*/
object ApiUser "alyvix-check" {
password = "${icinga_pwd}"
// client_cn = ""

permissions = [ "objects/query/Host", "objects/query/Service", "actions/process-check-result" ]
}
EOF

    chmod 644 "${icinga_pwd_file}"
    chown icinga.icinga "${icinga_pwd_file}"

    echo '[!] Done. Reload Icinga2 to enable the new API user account'
fi