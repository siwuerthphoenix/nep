#! /bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi



echo '[i] Creating Icinga Web 2 user account "alyvix-check"'
icingaweb_pwd_file="/root/.pwd_icingaweb2_alyvix_check"
if [ ! -f "${icingaweb_pwd_file}" ]; then
    echo '[!] Unable to get account password from pwd file'
    exit 1
fi

icingaweb_user='alyvix-check'
icingaweb_pwd="$(cat "${icingaweb_pwd_file}")"
icingaweb_pwd_hash="$(php -r "echo password_hash(\"${icingaweb_pwd}\", PASSWORD_DEFAULT);")"
RET=$?
if [ $RET -ne 0 ] ; then
    echo "  [-] Error while generating the hash of the password for the icingaweb2 user"
    exit 2
fi

if mysql icingaweb2 -e "INSERT INTO icingaweb_user (name, active, password_hash, ctime) VALUES('${icingaweb_user}', 1, '${icingaweb_pwd_hash}', now()) ON DUPLICATE KEY UPDATE password_hash='${icingaweb_pwd_hash}';"; then
    echo -e "[+] Account created"
else
    echo "[-] Error adding ${icingaweb_user} user"
    exit 3;
fi
