#! /bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh

# Generates password for alyvix-check account.
# This account is used by the provided monitoring plugins to:
# - Get data from Alyvix Testcases (using Icingaweb2 session)
# - Get the list of monitored Test cases (by querying Icinga2 API)
# - Set status of services based on Test cases status (by querying Icinga2 API)

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

username='alyvix-check'
icingaweb_role=''
icinga_pwd_file=".pwd_icinga2_alyvix_check"
icingaweb_pwd_file=".pwd_icingaweb2_alyvix_check"

if [[ -f "/root/$icinga_pwd_file" ]]; then
    echo "Password for Icinga2 account "alyvix-check" already exists. Skipping."
else
    echo '[i] Generating password for Icinga2 account "alyvix-check"'
    icinga_pwd=$(generate_and_save_pw "$icinga_pwd_file")
fi

if [[ -f "/root/$icingaweb_pwd_file" ]]; then
    echo "Password for Icinga Web 2 account "alyvix-check" already exists. Skipping."
else
    echo '[i] Generating password for Icinga Web 2 account "alyvix-check"'
    icingaweb_pwd=$(generate_and_save_pw "$icingaweb_pwd_file")
fi
