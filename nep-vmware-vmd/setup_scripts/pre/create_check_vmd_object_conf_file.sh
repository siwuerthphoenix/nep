#! /bin/bash
 
config_file_path=/neteye/shared/monitoring/plugins/check_vmd_object.conf
 
echo "[i] Adding Configuration file for check_vmd_object"
if [ -f ${config_file_path} ]
then
        echo " - Config file already present, skipping.";
else
        echo " - Creating Config file ${config_file_path}"
        cat << 'EOF' > ${config_file_path}
[VMDDB]
username = vspheredb
password = ChangeMe!
EOF
        echo " - Configuration file created. Please, remember to replace Username and Password inside it!"
        echo "[i] Done"
fi