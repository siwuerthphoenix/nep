. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh


SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi


## Remove Sync Rules
syncrule_objects=( "nx-sr-vmd-vcsa" "nx-sr-vmd-vcsa-create" "nx-sr-vmd-vcsa-update" "nx-sr-vmd-host-system" "nx-sr-vmd-host-system-create" "nx-sr-vmd-host-system-update" "nx-sr-vmd-virtual-machines" "nx-sr-vmd-virtual-machines-create" "nx-sr-vmd-virtual-machines-update" "nx-sr-vmd-datastore" )

for s in "${syncrule_objects[@]}"; do
    tmp=$(icingacli nep syncrule list | grep "$s")
    if [[ $tmp != "" ]]; then
        ## Import Source exist
        echo " - Removing Sync Rule object: ${s}"
        id=$(echo $tmp | awk '{ print $1 }')
        icingacli nep syncrule delete --id $id
    fi
done

## Remove Import Source
importsource_objects=( "nx-is-vmd-datastore" "nx-is-vmd-host-system" "nx-is-vmd-vcsa" "nx-is-vmd-virtual-machines" )

for s in "${importsource_objects[@]}"; do
    tmp=$(icingacli nep importsource list | grep "$s")
    if [[ $tmp != "" ]]; then
        ## Import Source exist
        echo " - Removing Import Source object: ${s}"
        id=$(echo $tmp | awk '{ print $1 }')
        icingacli nep importsource delete --id $id
    fi
done
