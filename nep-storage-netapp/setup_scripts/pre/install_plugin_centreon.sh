#!/bin/bash
## Install Plugin for Centeon

. /usr/share/neteye/scripts/rpm-functions.sh

dnf --enablerepo=epel install -y perl-CryptX
dnf --enablerepo=centreon-*-stable* install -y centreon-plugin-Hardware-Storage-Netapp-Ontap-Restapi
dnf --enablerepo=centreon-*-stable* install -y centreon-plugin-Hardware-Storage-Netapp-Ontap-Snmp


