const NxNotificationChannelPhoneCall = "phonecall"


template Notification "nx-nt-channel-phonecall" {
}

template Notification "nx-nt-channel-phonecall-for-host" {
        import "nx-nt-channel-phonecall"
        command = "nx-c-phonecall-notification"
}

template Notification "nx-nt-channel-phonecall-for-service" {
        import "nx-nt-channel-phonecall"
        command = "nx-c-phonecall-notification"
}


# Rules for Host Notification

apply Notification "nx-n-basic-phonecall-to-users-from-host" to Host {
	import "nx-nt-basic"
	import "nx-nt-channel-phonecall-for-host"

	users = host.vars.nx_notification_basic_users

	assign where match("enabled_for_host*", host.vars.nx_notification_basic_mode_host)
	ignore where !(NxNotificationChannelPhoneCall in host.vars.nx_notification_basic_channels)
	ignore where !(host.vars.nx_notification_basic_users)
}

apply Notification "nx-n-basic-phonecall-to-groups-from-host" to Host {
        import "nx-nt-basic"
        import "nx-nt-channel-phonecall-for-host"

        user_groups = host.vars.nx_notification_basic_groups

        assign where match("enabled_for_host*", host.vars.nx_notification_basic_mode_host)
        ignore where !(NxNotificationChannelPhoneCall in host.vars.nx_notification_basic_channels)
        ignore where !(host.vars.nx_notification_basic_groups)
}


# Rules for Service Notification

apply Notification "nx-n-basic-phonecall-to-users-from-host" to Service {
        import "nx-nt-basic"
        import "nx-nt-channel-phonecall-for-service"

        users = host.vars.nx_notification_basic_users

        if (service.vars.nx_notification_basic_users_override) {
                users = service.vars.nx_notification_basic_users_override
        }
        assign where verify_service_channel(host.vars.nx_notification_basic_channels, service.vars.nx_notification_basic_channels_override, NxNotificationChannelPhoneCall)
        ignore where !(match("enabled_for_*services", host.vars.nx_notification_basic_mode_host))
        ignore where !(match ("enabled", service.vars.nx_notification_basic_mode_service))
        ignore where !(host.vars.nx_notification_basic_users) && !(service.vars.nx_notification_basic_users_override)
}

apply Notification "nx-n-basic-phonecall-to-groups-from-hosts" to Service {
        import "nx-nt-basic"
        import "nx-nt-channel-phonecall-for-service"

        var Channel = false
        user_groups = host.vars.nx_notification_basic_groups

        if (service.vars.nx_notification_basic_groups_override) {
                user_groups = service.vars.nx_notification_basic_groups_override
        }
        assign where verify_service_channel(host.vars.nx_notification_basic_channels, service.vars.nx_notification_basic_channels_override, NxNotificationChannelPhoneCall)
        ignore where !(match("enabled_for_*services", host.vars.nx_notification_basic_mode_host))
        ignore where !(match ("enabled", service.vars.nx_notification_basic_mode_service))
        ignore where !(host.vars.nx_notification_basic_groups) && !(service.vars.nx_notification_basic_groups_override)
}


apply Notification "nx-n-basic-phonecall-to-users-from-service" to Service {
        import "nx-nt-basic"
        import "nx-nt-channel-phonecall-for-service"

        users = service.vars.nx_notification_basic_users_override

        assign where match("enabled_for_*services", host.vars.nx_notification_basic_mode_host) && match ("enabled", service.vars.nx_notification_basic_mode_service)
        ignore where !(NxNotificationChannelPhoneCall in host.vars.nx_notification_basic_channels)
        ignore where !(service.vars.nx_notification_basic_users_override)
}

apply Notification "nx-n-basic-phonecall-to-groups-from-service" to Service {
        import "nx-nt-basic"
        import "nx-nt-channel-phonecall-for-service"

        user_groups = service.vars.nx_notification_basic_groups_override

        assign where match("enabled_for_*services", host.vars.nx_notification_basic_mode_host) && match ("enabled", service.vars.nx_notification_basic_mode_service)
        ignore where !(NxNotificationChannelPhoneCall in host.vars.nx_notification_basic_channels)
        ignore where !(service.vars.nx_notification_basic_groups_override)
}
