. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

channel="phonecall"
echo "Create User Template for '$channel' channel"

## Check exist and create
tmp=$(icingacli director user exist "nx-ut-$channel")
if [[ $tmp == *"does not"* ]]; then
    echo " - Add user template 'nx-ut-$channel'"
    icingacli director user create "nx-ut-$channel" --object_type template \
        --enable_notifications \
        --states "Critical" --states  "Down" \
        --types "Custom" --types "Problem"
fi


