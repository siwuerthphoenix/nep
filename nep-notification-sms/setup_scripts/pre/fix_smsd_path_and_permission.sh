. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

smssend_dir="/neteye/local/smsd/data/spool"

function update_smssend_dir {
    config_file="/usr/bin/smssend"

    sed -i 's|/var/spool/sms/|'${smssend_dir}'|g' ${config_file}
}

echo "Updating SMSd Spool definition"
# Creating PluginContribDir

if [ ! -d ${smssend_dir} ] ; then
    echo " - Creating SMSd Spool dir"
    mkdir -p ${smssend_dir}
fi

# Updating PluginContribDir on Icinga2 Local instance
echo " - Updating Local SMSd instance"
update_smssend_dir

echo " - Updating Owner SMSd spool"
chown icinga:icinga /neteye/local/smsd/data/spool/*