#!/bin/bash
## 

if [ -f "/etc/sudoers.d/SMcli" ]; then
    echo "SMcli already on sudoers."
    exit 0
fi

cat << EOF > /etc/sudoers.d/SMcli
icinga ALL = NOPASSWD: /opt/IBM_DS/client/SMcli
icinga ALL = NOPASSWD: /usr/lib/centreon/plugins/centreon_ibm_ds3000.pl
EOF