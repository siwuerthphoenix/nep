#!/bin/bash
## Install Plugin for Centeon

. /usr/share/neteye/scripts/rpm-functions.sh

dnf --enablerepo=centreon-*-stable* --enablerepo=epel install -y centreon-plugin-Hardware-Storage-Ibm-Storwize-Ssh
if [ $? -eq 0 ]; then
    echo ' Done'
else
    echo ' Unable to install RPM centreon-plugin-Hardware-Storage-Ibm-Storwize-Ssh'
    exit 1
fi

dnf --enablerepo=centreon-*-stable* --enablerepo=epel install -y centreon-plugin-Hardware-Storage-Ibm-Ds3000-Smcli
if [ $? -eq 0 ]; then
    echo ' Done'
else
    echo ' Unable to install RPM centreon-plugin-Hardware-Storage-Ibm-Ds3000-Smcli'
    exit 1
fi
