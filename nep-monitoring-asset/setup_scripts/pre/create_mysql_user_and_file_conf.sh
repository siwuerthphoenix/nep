
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo "Creating MySQL Read Only user to Monitoring Asset"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

mysql_username='assetmanagement'
mysql_pwd_file="/root/.pwd_$mysql_username"

mysql_host="mariadb.neteyelocal"
mysql_port=3306


if [ -f "$mysql_pwd_file" ]; then
    echo "$mysql_pwd_file already exists. Skip."
else
    mysql_password=$(generate_and_save_pw "$mysql_username")
    echo " - Creating Database User for access"
    cat << EOF | mysql
CREATE USER IF NOT EXISTS '${mysql_username}'@'%' IDENTIFIED BY '${mysql_password}';
CREATE USER IF NOT EXISTS'${mysql_username}'@'localhost' IDENTIFIED BY '${mysql_password}';
GRANT SELECT on glpi.* to '${mysql_username}'@'localhost';
GRANT SELECT on ocsweb.* to '${mysql_username}'@'localhost';	
GRANT SELECT on glpi.* to '${mysql_username}'@'%';
GRANT SELECT on ocsweb.* to '${mysql_username}'@'%';
FLUSH PRIVILEGES;
EOF

    echo "Create Asset config file"
    cat << EOF > /neteye/shared/monitoring/plugins/check_assetmanagement.conf
\$mariadb_host = "${mysql_host}";

\$ocs_db = "ocsweb";
\$ocs_user = "${mysql_username}";
\$ocs_pass = "${mysql_password}";

\$glpi_db ="glpi";
\$glpi_user = "${mysql_username}";
\$glpi_pass = "${mysql_password}";
EOF

    if is_cluster ;then
        echo "[i] sync conf file on all nodes (exclude voting)"
        nodes=$(get_cluster_nodes_without_voting_hostname)

        for node in $nodes ; do rsync -avh /neteye/shared/monitoring/plugins/check_assetmanagement.conf $node:/neteye/shared/monitoring/plugins/ ; done
    fi
fi
