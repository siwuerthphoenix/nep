. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo "[i] Prparing to update/rebuild Service Sets definitions"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

SQL="SELECT h.object_name AS 'Object Name', h.object_type AS 'Object Type' FROM icinga_service_set ssc INNER JOIN icinga_service_set_inheritance ssh ON ssh.service_set_id = ssc.id INNER JOIN icinga_service_set ssp ON ssh.parent_service_set_id = ssp.id INNER JOIN icinga_host h ON ssc.host_id = h.id WHERE ssc.object_name = "
serviceset_objects=( "nx-ss-ethernetbox" "nx-ss-network-ethernetbox" )

echo " - Checking direct assignment for Service Sets: ${serviceset_objects}"

## Delete service set objects
for s in "${serviceset_objects[@]}"; do
    tmp=$(icingacli director serviceset exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        ## Retrive manual usage of service set
        response=$(mysql -D director -e "$SQL'$s'")
        if [ -n "$response" ]; then
            echo "------------------------------------------------------------------------------------"
            echo "WARNING: Service Set '$s' assigned manually to these Director objects"
            mysql -D director -e "$SQL'$s'"
            echo "You have to add manually this Service Set and then deploy your configuration"
        fi
        ## Delete service set objects
        icingacli director serviceset delete "$s"
    fi
done