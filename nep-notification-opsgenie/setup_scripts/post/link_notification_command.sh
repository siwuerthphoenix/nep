#! /bin/bash
## Add Notification Plugin to Icinga2 Master Instance directories
## It is done by a symlink

. /usr/share/neteye/scripts/rpm-functions.sh

# This kind of configuration must be performed only where Icinga2 Master is running

DRBD_MOUNTPOINT="icinga2"
if is_cluster && ! is_drbd_mounted  "$DRBD_MOUNTPOINT" ; then
    echo "[i] Inactive Cluster Node, skipping autosetup"
    exit 0
fi

OEC_USER=opsgenie
OEC_GROUP=$OEC_USER
JEC_USER=jec
JEC_GROUP=$OEC_USER
PLUGIN_BASE_PATH=/neteye/shared/icinga2/scripts
OEC_PLUGIN_PATH=/neteye/shared/icinga2/scripts/opsgenie-icinga2
JEC_PLUGIN_PATH=/neteye/shared/icinga2/scripts/jsm-opsgenie-icinga2

echo '[i] Creating Icinga2 scripts folder'
if [ ! -d "${PLUGIN_BASE_PATH}" ]; then
    mkdir $PLUGIN_BASE_PATH
fi

echo '[i] Creating symlink to Notification Script for Icinga2'
if [ ! -d "${OEC_PLUGIN_PATH}" ]; then
    ln -s -f `eval echo ~${OEC_USER}`/oec/opsgenie-icinga2/send2opsgenie "${OEC_PLUGIN_PATH}"
fi

if [ ! -d "${JEC_PLUGIN_PATH}" ]; then
    ln -s -f `eval echo ~${JEC_USER}`/jec/scripts/send2jsm "${JEC_PLUGIN_PATH}"
fi