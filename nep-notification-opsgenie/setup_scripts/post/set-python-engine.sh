#! /bin/bash

# Action scripts are launched by OEC by directly invoking 'python'.
# Since on RHEL8 the default symlink python is not set, it now will be
# forced via alternatives.

PYTHON3_ENGINE_PATH=/usr/bin/python3

echo "Checking default python engine"
python_alternatives=$(alternatives --list | egrep '^python ')
alternatives_mode=$(echo ${python_alternatives} | cut -d ' ' -f 2)
alternatives_path=$(echo ${python_alternatives} | cut -d ' ' -f 3)
echo "[i] Current mode is \"${alternatives_mode}\""
echo "[i] Current python engine is \"${alternatives_path}\""

if [[ "${alternatives_mode}" == "auto" ]]; then
    echo "[i] Current alternative for Python is automatic. Forcing it to Python 3..."
    alternatives --set python ${PYTHON3_ENGINE_PATH}
else
    echo "[i] Current alternative for Python as been already set. Skipping."
fi