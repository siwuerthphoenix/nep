#! /bin/bash
## Prepare configuration file template for JEC
## PREREQUISITE: Jira Edger Connector Password for Icinga must be created (during pre-setup phase)

. /usr/share/neteye/scripts/rpm-functions.sh

# This kind of configuration must be performed on all cluster nodes

JEC_PWD_FILE=.pwd_icinga_jec
JEC_CFG_TEMPLATE_FILE=/neteye/local/jec/conf/neteye-jec.json.tpl

# Read Icinga2 API Password from file: it has been generated during pre-setup phase
if [ -f "/root/$JEC_PWD_FILE" ]; then
    JEC_PWD=$(cat "/root/$JEC_PWD_FILE")
else
    echo "Unable to read Jira Edge Connector password for Icinga"
    exit 1
fi

# Update password in JEC Configuration template file
echo "Updating Configuration template file for Jira Edge Connector"
template=$(jq " .\"globalFlags\".\"password\" = \"$JEC_PWD\"" $JEC_CFG_TEMPLATE_FILE)
if [ "$?" == "0" ]; then
    echo "$template" > $JEC_CFG_TEMPLATE_FILE
else
    echo "Unable to update Jira Edge Connector Template file"
    exit 1
fi
