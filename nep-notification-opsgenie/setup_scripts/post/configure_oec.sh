#! /bin/bash
## Prepare configuration file template for OEC
## PREREQUISITE: Opsgenie Edger Connector Password for Icinga must be created (during pre-setup phase)

. /usr/share/neteye/scripts/rpm-functions.sh

# This kind of configuration must be performed on all cluster nodes

OEC_PWD_FILE=.pwd_icinga_oec
OEC_CFG_TEMPLATE_FILE=/neteye/local/oec/conf/neteye-oec.json.tpl

# Read Icinga2 API Password from file: it has been generated during pre-setup phase
if [ -f "/root/$OEC_PWD_FILE" ]; then
    OEC_PWD=$(cat "/root/$OEC_PWD_FILE")
else
    echo "Unable to read Opsgenie Edge Connector password for Icinga"
    exit 1
fi

# Update password in OEC Configuration template file
echo "Updating Configuration template file for Opsgenie Edge Connector"
template=$(jq " .\"globalFlags\".\"password\" = \"$OEC_PWD\"" $OEC_CFG_TEMPLATE_FILE)
if [ "$?" == "0" ]; then
    echo "$template" > $OEC_CFG_TEMPLATE_FILE
else
    echo "Unable to update Opsgenie Edge Connector Template file"
    exit 1
fi
