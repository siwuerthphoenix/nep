#! /bin/bash
## Copy (or update) OPSGenie/Jira Notification Plugin in to Icinga2 directories
## PREREQUISITE: Opsgenie Edger Connector and Jira Edge Connector must be installed on the system

. /usr/share/neteye/scripts/rpm-functions.sh

# This kind of configuration must be performed only where Icinga2 Master is running

DRBD_MOUNTPOINT="icinga2"
if is_cluster && ! is_drbd_mounted  "$DRBD_MOUNTPOINT" ; then
    echo "[i] Inactive Cluster Node, skipping autosetup"
    exit 0
fi

OEC_PLUGIN_NAME=send2opsgenie
JEC_PLUGIN_NAME=send2jsm
OEC_RPM_BIN_PATH=/home/opsgenie/oec/opsgenie-icinga2/${OEC_PLUGIN_NAME}
JEC_RPM_BIN_PATH=/home/jsm/jec/scripts/${JEC_PLUGIN_NAME}
DESTINATION_PATH=/neteye/shared/icinga2/conf/icinga2/scripts

echo "[i] Installing/updating OPSGenie/Jira notification plugin"

if [ ! -d "${DESTINATION_PATH}" ]; then
    mkdir "${DESTINATION_PATH}"
    chown icinga.icinga "${DESTINATION_PATH}"
fi

/usr/bin/cp -f "${OEC_RPM_BIN_PATH}" "${DESTINATION_PATH}/"
/usr/bin/cp -f "${JEC_RPM_BIN_PATH}" "${DESTINATION_PATH}/"
chmod +x "${DESTINATION_PATH}/${OEC_PLUGIN_NAME}"
chmod +x "${DESTINATION_PATH}/${JEC_PLUGIN_NAME}"
