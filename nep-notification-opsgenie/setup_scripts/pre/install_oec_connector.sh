#!/bin/bash
## Install and configure OpsGenie Edge Connector for NetEye
# This must be done on all cluster nodes (if is a cluster)

# Common variables
OEC_NE_DIR=/neteye/local/oec
OEC_NE_CFG_DIR=$OEC_NE_DIR/conf
OEC_NE_LOG_DIR=$OEC_NE_DIR/log
OEC_NE_SYS_DIR=$OEC_NE_DIR/conf/sysconfig
OEC_USER=opsgenie
OEC_GROUP=$OEC_USER
OEC_UNIT_NAME=oec.service
ICINGA_USER=icinga
RPM_NAME='opsgenie-icinga2'
RPM_URL='https://github.com/opsgenie/oec-scripts/releases/download/Icinga2-1.1.6_oec-1.1.3/opsgenie-icinga2-1.1.6.x86_64.rpm'


## To get the list of all available version, use https://github.com/atlassian/jsm-integration-scripts/releases
echo "Installing/Updating OpsGenie integration with Icinga2"
if (rpm -q $RPM_NAME > /dev/null); then
    echo "RPM $RPM_NAME already installed"
else
    echo "Installing RPM $RPM_NAME"
    dnf -y install "$RPM_URL"

    echo "Stopping and disabling JEC Service"
    systemctl disable --now $OEC_UNIT_NAME    
fi

if [ ! -d $OEC_NE_DIR ]; then
    echo 'Preparing basic configuration for OpsGenie Edge Connector/NetEye integration'

    # Create folders with the right permissions
    mkdir -p $OEC_NE_DIR
    mkdir -p $OEC_NE_CFG_DIR
    mkdir -p $OEC_NE_LOG_DIR
    mkdir -p $OEC_NE_SYS_DIR
    chown -R $OEC_USER.$OEC_GROUP $OEC_NE_DIR

    # Allow user icinga to access oec data
    usermod -a -G $OEC_GROUP $ICINGA_USER

else
    echo 'OpsGenie Edge Connector already configured'
fi

