#! /bin/bash
## Create and save a password used by Jira Edge Connector to access Icinga

. /usr/share/neteye/scripts/rpm-functions.sh

# This kind of configuration must be performed only where Icinga2 Master is mounted

DRBD_MOUNTPOINT="icinga2"
if is_cluster && ! is_drbd_mounted  "$DRBD_MOUNTPOINT" ; then
    echo "[i] Inactive Cluster Node, skipping autosetup"
    exit 0
fi

JEC_PWD_FILE=.pwd_icinga_jec
if [ ! -f "/root/$JEC_PWD_FILE" ]; then
    echo "Generating Jira Edge Connector password for Icinga"
    generate_and_save_pw "$JEC_PWD_FILE"
fi