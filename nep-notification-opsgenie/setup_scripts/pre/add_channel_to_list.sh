#!/bin/bash
## Add channel to list
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

DRBD_MOUNTPOINT="icingaweb2"
if is_cluster && ! is_drbd_mounted  "$DRBD_MOUNTPOINT" ; then
    echo "[i] Inactive Cluster Node, skipping autosetup"
    exit 0
fi

FILE="/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/nx-channel-list.csv"

function add_channel_to_list() {
    key="$1"
    name="$2"

    if grep -q "^${key}," $FILE ; then
        echo "Channel '$key' already present... Nothing to do."
    else
cat << EOF >> $FILE
$key,$name,string,null
EOF
    fi
}

# For compatibility, add channels for both OpsGenie Standalone and OpsGenie as JSM Module
add_channel_to_list "opsgenie" "OpsGenie"
add_channel_to_list "jsm-opsgenie" "JSM via OpsGenie"

