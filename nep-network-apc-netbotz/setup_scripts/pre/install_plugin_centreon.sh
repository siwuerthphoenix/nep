#!/bin/bash
## Install Plugin for Centeon

dnf --enablerepo=centreon-*-stable* install -y centreon-plugin-Hardware-Sensors-Netbotz-Snmp
if [ $? -eq 0 ]; then
    echo ' Done'
else
    echo ' Unable to install RPM centreon-plugin-Hardware-Sensors-Netbotz-Snmp'
    exit 1
fi