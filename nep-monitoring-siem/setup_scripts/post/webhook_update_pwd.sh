
. /usr/share/neteye/scripts/rpm-functions.sh

SERVICE="tornado_webhook_collector"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

# For Elastic Dataset
WEBHOOK_FILE="/neteye/shared/tornado_webhook_collector/conf/webhooks/nx-elastic-dataset.json"
PASSWORD_FILE=".pwd_webhook_nx_elastic_dataset"

PASSWORD=$(cat "/root/${PASSWORD_FILE}")
PAYLOAD=$(cat "${WEBHOOK_FILE}")
echo "${PAYLOAD}" | sed "s/@@PASSWORD@@/${PASSWORD}/g" > $WEBHOOK_FILE
EXIT="$?"
if [ "${EXIT}" -ne 0 ] ; then
    echo "  [-] Impossible to set the password for nx_elastic_dataset"
    exit 1
fi

# For Elastic Transforms
WEBHOOK_FILE="/neteye/shared/tornado_webhook_collector/conf/webhooks/nx-elastic-transforms.json"
PASSWORD_FILE=".pwd_webhook_nx_elastic_transforms"

PASSWORD=$(cat "/root/${PASSWORD_FILE}")
PAYLOAD=$(cat "${WEBHOOK_FILE}")
echo "${PAYLOAD}" | sed "s/@@PASSWORD@@/${PASSWORD}/g" > $WEBHOOK_FILE
EXIT="$?"
if [ "${EXIT}" -ne 0 ] ; then
    echo "  [-] Impossible to set the password for nx_elastic_transforms"
    exit 1
fi

# For EBP Verify (must create one for each blockchain in /etc/neteye-dpo)
WEBHOOK_FILE="/neteye/shared/tornado_webhook_collector/conf/webhooks/elproxy_verification.json"
PASSWORD_FILE=".pwd_webhook_nx_elproxy_verification"

PASSWORD=$(cat "/root/${PASSWORD_FILE}")
PAYLOAD=$(cat "${WEBHOOK_FILE}")
echo "${PAYLOAD}" | sed "s/@@PASSWORD@@/${PASSWORD}/g" > $WEBHOOK_FILE
EXIT="$?"
if [ "${EXIT}" -ne 0 ] ; then
    echo "  [-] Impossible to set the password for nx_elproxy_verification"
    exit 1
fi


echo "[i] Restart tornado_webhook_collector service"
systemctl restart tornado_webhook_collector.service