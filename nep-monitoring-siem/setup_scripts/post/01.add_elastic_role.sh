. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi


ES_CURL_DIR="/usr/share/neteye/elasticsearch/scripts/"
QUERY_FILE="/tmp/role.json"

function check_api_response {
  STATUS=$(echo $1 | jq 'select(.ErrorCode!=null).ErrorCode')
  RET=$2

  if [[ "$RET" -ne "0" ]]
  then
    echo "[!] curl returned exit code $RET"
    exit 1
  fi

  if [[ "$STATUS" -ne "200" ]]
  then
    echo "[!] Elasticsearch returned the following error:"
    echo $CURL_RAW_RESPONSE | jq 'select(.error!=null).error'
    exit 1
  fi
}


#### MAIN
ROLE_NAME="neteye_director_ingest_check"
cat << EOF > "$QUERY_FILE"
{
    "cluster" : [ "manage_slm" ],
    "indices" : [
      {
        "names" : [
          "logs-*",
          "metrics-*",
          "logstash-*",
          "*beat-*"
        ],
        "privileges" : [
          "read",
          "monitor"
        ],
        "field_security" : {
          "grant" : [
            "*"
          ],
          "except" : [ ]
        },
        "allow_restricted_indices" : false
      }
    ],
    "applications" : [ ],
    "run_as" : [ ],
    "metadata" : { "owned_by": "NetEye" },
    "transient_metadata" : {
      "enabled" : true
    }
}
EOF

CURL_RAW_RESPONSE="$(${ES_CURL_DIR}/es_curl.sh -sS -w '{"ErrorCode": %{http_code}}' -H 'Content-Type: application/json' -X PUT "https://elasticsearch.neteyelocal:9200/_security/role/${ROLE_NAME}" --data-binary "@${QUERY_FILE}")"

check_api_response "$CURL_RAW_RESPONSE" "$?"
echo "[i] Role added succesfully to Elastic '$ROLE_NAME'"

####
## Add role_mapping
####
cat << EOF > "$QUERY_FILE"
{
    "enabled" : true,
    "roles" : [
      "$ROLE_NAME"
    ],
    "rules" : {
      "field" : {
        "username" : "NetEyeElasticCheck"
      }
    },
    "metadata" : { "owned_by": "NetEye" }
}
EOF

CURL_RAW_RESPONSE="$(${ES_CURL_DIR}/es_curl.sh -sS -w '{"ErrorCode": %{http_code}}' -H 'Content-Type: application/json' -X PUT "https://elasticsearch.neteyelocal:9200/_security/role_mapping/${ROLE_NAME}" --data-binary "@${QUERY_FILE}")"

check_api_response "$CURL_RAW_RESPONSE" "$?"
echo "[i] Role mapping added succesfully to Elastic '$ROLE_NAME'"

####
# Add fleet role
####
ROLE_NAME="fleet_role"
cat << EOF > "$QUERY_FILE"
{
    "cluster" : [ ],
    "indices" : [ ],
    "applications": [
      {
        "application": "kibana-.kibana",
        "privileges": [
          "feature_api.all",
          "feature_fleet.read",
          "feature_fleetv2.all",
          "feature_siem.minimal_read",
          "feature_siem.endpoint_list_read"
        ],
        "resources": [
          "*"
        ]
      }
    ],
    "run_as" : [ ],
    "metadata" : { "owned_by": "NetEye" },
    "transient_metadata" : {
      "enabled" : true
    }
}
EOF
CURL_RAW_RESPONSE="$(${ES_CURL_DIR}/es_curl.sh -sS -w '{"ErrorCode": %{http_code}}' -H 'Content-Type: application/json' -X PUT "https://elasticsearch.neteyelocal:9200/_security/role/${ROLE_NAME}" --data-binary "@${QUERY_FILE}")"

check_api_response "$CURL_RAW_RESPONSE" "$?"
echo "[i] Role added succesfully to Elastic '$ROLE_NAME'"
