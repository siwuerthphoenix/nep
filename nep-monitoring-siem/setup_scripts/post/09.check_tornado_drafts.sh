#!/usr/bin/env bash
### Check if torando drafts are present
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="tornado"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

DRAFT_PATH="/neteye/shared/tornado/conf/drafts/draft_001/config/master"
RULES_PATH="/neteye/shared/tornado/conf/rules.d/master"

if [ -d "$DRAFT_PATH" ]; then
    cp -pa  ${RULES_PATH}/nx_elastic_dataset $DRAFT_PATH
    cp -pa  ${RULES_PATH}/nx_elastic_transforms $DRAFT_PATH
    cp -pa  ${RULES_PATH}/elproxy_verification $DRAFT_PATH
    echo "[+] Add tornado rules to open draft"
fi