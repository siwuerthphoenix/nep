###############
#### Add user for API monitoring
###############
## NOTE work only from neteye 4.25
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icinga2-master"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi


API_NAME="icinga-monitoring"
FILE=/neteye/shared/icinga2/conf/.elastic_api_key
JSON_INPUT='
{
  "name": "'${API_NAME}'",
  "role_descriptors": { 
    "monitor": {
      "cluster": ["monitor"]
    }
  },
  "metadata": {
    "application": "icinga",
    "environment": {
       "level": 1,
       "trusted": true
    }
  }
}
'

## Check API user exists
CURL_RAW_RESPONSE="$(/usr/share/neteye/elasticsearch/scripts/es_curl.sh -sS -w '{"ErrorCode": %{http_code}}' -XGET https://elasticsearch.neteyelocal:9200/_security/api_key?name=${API_NAME} )"

result=$(echo $CURL_RAW_RESPONSE | jq 'select(.api_keys!=null).api_keys')
status=$(echo $CURL_RAW_RESPONSE | jq 'select(.ErrorCode!=null).ErrorCode')

if [[ $status -ne 200 ]]
then
  echo $CURL_RAW_RESPONSE | jq 'select(.error!=null).error'
  exit 1
fi

if [[ -n $result ]] && [[ $result == "[]" ]]; then
   echo "Create the new api key"
    ## Add API user
    CURL_RAW_RESPONSE="$(/usr/share/neteye/elasticsearch/scripts/es_curl.sh -sS -w '{"ErrorCode": %{http_code}}' -XPOST 'https://elasticsearch.neteyelocal:9200/_security/api_key' -H 'Content-Type: application/json' -d "$JSON_INPUT" )"


    if [[ $status -ne 200 ]]
    then
    echo $CURL_RAW_RESPONSE | jq 'select(.error!=null).error'
    exit 1
    else
    ##{"id":"W3pCo4MByA8i2Wf0oW2D","name":"${API_NAME}","api_key":"F2wZTsIYQbKRxc5BZ9YUWA","encoded":"VzNwQ280TUJ5QThpMldmMG9XMkQ6RjJ3WlRzSVlRYktSeGM1Qlo5WVVXQQ=="}{"ErrorCode": 200}
    echo "API keys with name '${API_NAME}' created successfull!"
    ## Write API on auth_file
    echo $CURL_RAW_RESPONSE | jq 'select(.encoded!=null)' > $FILE
    exit 0
    fi

else
    echo "API key with name '${API_NAME}' already exist. Nothing to do."
fi