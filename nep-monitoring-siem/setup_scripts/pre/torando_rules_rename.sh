. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="tornado"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

## Rename folder before 4.39
if [ -d '/neteye/shared/tornado/conf/rules.d/master/elproxy_verification' ];then
    echo "[w] Existing old Tornado rules 'elproxy_verification'. Migration..."
    rm -f /neteye/shared/tornado/conf/rules.d/master/elproxy_verification/elproxy_verification_ruleset/0000000010_elproxy_verification.json /neteye/shared/tornado/conf/rules.d/master/elproxy_verification/elproxy_verification_ruleset/0000000020_elproxy_duplicates_removal.json
    mv /neteye/shared/tornado/conf/rules.d/master/elproxy_verification/elproxy_verification_ruleset /neteye/shared/tornado/conf/rules.d/master/elproxy_verification/nx_elproxy_verification_rules
    mv /neteye/shared/tornado/conf/rules.d/master/elproxy_verification /neteye/shared/tornado/conf/rules.d/master/nx_elproxy_verification
    echo "[i] Migration old Tornado rules complete!"
fi