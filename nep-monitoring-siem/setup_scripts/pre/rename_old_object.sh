. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

declare -A command_objects
command_objects["nx-c-check_es_system"]="nx-c-check-es-system"
command_objects["nx-c-check_filebeat"]="nx-c-check-filebeat"
command_objects["nx-c-check_logstash"]="nx-c-check-logstash"
command_objects["nx-c-check_logstash_queue"]="nx-c-check-logstash-queue"

echo "Removing legacy Director Objects"

## Rename command objects
for c in "${!command_objects[@]}"; do
    tmp=$(icingacli director command exist "$c")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director command set "$c" --object_name "${command_objects[$c]}"
    fi
done


declare -A set_objects
set_objects["nx-ss-neteye-siem-state"]="nx-ss-neteye-siem-units-state"
set_objects["nx-ss-neteye-siem-clustered-state"]="nx-ss-neteye-siem-cluster-state"

## Rename serviceset objects
for s in "${!set_objects[@]}"; do
    tmp=$(icingacli director serviceset exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director serviceset set "$s" --object_name "${set_objects[$s]}"
    fi
done