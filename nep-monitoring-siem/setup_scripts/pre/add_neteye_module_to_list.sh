#!/bin/bash
## Add vendor to list
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

key="neteye-siem"
name="NetEye SIEM"
FILE="/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/nx-neteye-modules-list.csv"

if grep -q "^${key}," $FILE ; then
    echo "NetEye Module '$key' already present... Nothing to do."
else
cat << EOF >> $FILE
$key,$name,string,null
EOF

fi
