## For Elastic Dataset Password
PASSWORD_FILE=".pwd_webhook_nx_elastic_dataset"

FILE="/usr/share/neteye/nep/nep-monitoring-siem/baskets/import/nep-monitoring-siem-05-serviceset.json"

PASSWORD=$(cat "/root/${PASSWORD_FILE}")
PAYLOAD=$(cat "${FILE}")
echo "${PAYLOAD}" | sed "s/@@PASSWORD@@/${PASSWORD}/g" > $FILE
EXIT="$?"
if [ "${EXIT}" -ne 0 ] ; then
    echo "  [-] Impossible to set nx_elastic_dataset password"
    exit 1
fi

## For Elastic Transforms Password
PASSWORD_FILE=".pwd_webhook_nx_elastic_transforms"

PASSWORD=$(cat "/root/${PASSWORD_FILE}")
PAYLOAD=$(cat "${FILE}")
echo "${PAYLOAD}" | sed "s/@@PASSWORD_TRANSFORMS@@/${PASSWORD}/g" > $FILE
EXIT="$?"
if [ "${EXIT}" -ne 0 ] ; then
    echo "  [-] Impossible to set nx_elastic_transforms password"
    exit 1
fi

## For EBP Verify
PASSWORD_FILE=".pwd_webhook_nx_elproxy_verification"

PASSWORD=$(cat "/root/${PASSWORD_FILE}")
PAYLOAD=$(cat "${FILE}")
echo "${PAYLOAD}" | sed "s/@@PASSWORD_EBP_VERIFY@@/${PASSWORD}/g" > $FILE
EXIT="$?"
if [ "${EXIT}" -ne 0 ] ; then
    echo "  [-] Impossible to set nx_elproxy_verification password"
    exit 1
fi


exit 0