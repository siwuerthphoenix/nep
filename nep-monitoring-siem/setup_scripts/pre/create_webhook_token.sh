#!/bin/bash
. /usr/share/neteye/scripts/rpm-functions.sh

SERVICE="tornado_webhook_collector"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

PASSWORD_FILE=".pwd_webhook_nx_elastic_dataset"

if [ ! -f "/root/${PASSWORD_FILE}" ] ; then
    generate_and_save_pw $PASSWORD_FILE
    echo "[i] Adding token on Tornado Webhook 'nx-elastic-dataset'"
else
   echo "[i] Token already exist for Tornado Webhook 'nx-elastic-dataset'.. skip"
fi

# For Elasticsearch Transforms
PASSWORD_FILE=".pwd_webhook_nx_elastic_transforms"

if [ ! -f "/root/${PASSWORD_FILE}" ] ; then
    generate_and_save_pw $PASSWORD_FILE
    echo "[i] Adding token on Tornado Webhook 'nx-elastic-transforms'"
else
   echo "[i] Token already exist for Tornado Webhook 'nx-elastic-transforms'.. skip"
fi

# For EBP Verify
PASSWORD_FILE=".pwd_webhook_nx_elproxy_verification"

if [ ! -f "/root/${PASSWORD_FILE}" ] ; then
    generate_and_save_pw $PASSWORD_FILE
    echo "[i] Adding token on Tornado Webhook 'nx_elproxy_verification'"
else
   echo "[i] Token already exist for Tornado Webhook 'nx_elproxy_verification'.. skip"
fi

exit 0