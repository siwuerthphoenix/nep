# NEP Monitoring SIEM
The `nep-monitoring-siem` package is the NEP designed to monitoring the NetEye SIEM modules. It provides a quite complete monitoring for all the components of this module. With `nep-monitoring-siem` it is possible to perform monitoring of:
* Elasticsearch System (more info on [Official Documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/elasticsearch-intro.html)).
* Logstash System (more info on [Official Documentation](https://www.elastic.co/guide/en/logstash/current/introduction.html)).
* Filebeat System (more info on [Official Documentation](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-overview.html)).

# Table of Contents
1. [Prerequisites](#prerequisites)
2. [Installation](#installation)
3. [Packet Contents](#packet-contents)
4. [Usage](#usage)


## Prerequisites

| Software Version    | Version |
| ------------------  | ------- |
| NetEye              | 4.31    |
| nep-common          | 0.1.0   |
| nep-monitoring-core | 0.0.6   |


##### Required NetEye Modules

| NetEye Module |
| ------------  |
| Core          |
| SIEM          |


### External dependencies

* Ruby: Version 2.0+


## Installation

#### Before Installation

There is no need to perform any action before installing this NEP


### NEP Installation

To install the `nep-monitoring-siem`, use `nep-setup` via SSH on NetEye Master Node:
```bash
nep-setup install nep-monitoring-siem
```


#### Finalizing Installation

There is no need to perform any action to complete the installation of this NEP


## Packet Contents

### Director/Icinga Objects

This NEP provide the follwoing Director objects.


#### Host Templates

This NEP doesn't provide any Host Template definition


#### Data Lists

The following Data Lists can be freely customized by the End User. Their purpose is to provide easy data filling to better describe the monitoring environment.

| Data List name                | Description        |
| ----------------------------  | ------------------ |
| [NX] Elastic Check Types List | Used to provide the list of checks available for `nx-c-check-es-system` command |
| [NX] Kibana Check Types List  | Used to provide the list of checks available for `nx-c-check-kibana-stats` command |
| [NX] Elastic Agent Types List | Used to provide the list of agent type available (Filebeat, Metricbeat, Packetbeat, Winlogbeat, Auditbeat, Logstash, Elastic Agent) |


#### Service Templates

The following Service Templates can be used to freely create Service Objects, Service Apply Rules or Service Sets. 

_Remember to not edit these Service Templates as they will be restored/updated at the next NEP Package update_:

* `nx-st-agent-elastic`
* `nx-st-agent-filebeat`
* `nx-st-agent-filebeat-neteye`
* `nx-st-agent-logstash`
* `nx-st-agent-logstash-events`
* `nx-st-agent-logstash-neteye`
* `nx-st-agent-logstash-queue`
* `nx-st-agent-kibana`
* `nx-st-agentless-restapi-elastic-ingest-status`
* `nx-st-agentless-elastic-agent-check`
* `nx-st-agentless-elastic-endpoint-check`
* `nx-st-fleet-agent-status`
* `nx-st-endpoint-agent-status`
* `nx-st-agentless-elastic-slm-policy`
* `nx-st-agentless-elastic-slm-status`
* `nx-st-agentless-elastic-dataset-status`
* `nx-st-interval-10m`
* `nx-st-agentless-elastic-transforms-status`


#### Services Sets

The following Service Sets can be used to freely monitor Host Objects.

_Remember to not edit these Service Sets because they will be restored/updated at the next NEP Package update_:

* `nx-ss-agent-linux-elastic-agent-state`: Service Set providing common monitoring for Elastic Agent Service on NetEye
  * Elastic Agent Status
  * Unit Elastic Agent State
* `nx-ss-agent-windows-elastic-agent-state`: Service Set providing common monitoring for Elastic Agent Service on Windows System
  * Elastic Agent Status
  * Service Elastic Agent Status
* `nx-ss-agent-linux-elastic-endpoint-state`: Service Set providing common monitoring for Elastic Endpoint Service on Linux System
  * Elastic Endpoint Status
  * Unit Elastic Endpoint State
* `nx-ss-agent-windows-elastic-endpoint-state`: Service Set providing common monitoring for Elastic Endpoint Service on Windows System
  * Elastic Endpoint Status
  * Service Elastic Endpoint State
* `nx-ss-neteye-siem-disks-state`: Service Set providing common monitoring for Disk Space on a cluster for NetEye SIEM Module
  * Disk Tornado RSyslog collector Free space
  * Disk FileBeat Free space
  * Disk Elastic Blockchain Proxy Free space
  * Disk Kibana-Logmanager Free space
  * Disk Logstash Free space
  * Disk RSyslog-Logmanager Free space
* `nx-ss-neteye-siem-services-state`: Service Set providing common monitoring for NetEye SIEM Module on a single and cluster master
  * Elastic Cluster Status
  * Elastic Cluster Master
  * Elastic Cluster Max Shards
  * Elastic Cluster Disk Usage
  * Elastic Cluster Memory Usage
  * Filebeat Status
  * Logstash Status
  * Logstash Total EPS
  * Logstash Queue EBP Status
  * Kibana Max Response Time
  * Kibana Task Manager Status
  * Elastic Dataset Status - Master
* `nx-ss-neteye-siem-units-state`: Service Set providing common monitoring for NetEye SIEM Module on a single and cluster master
  * Unit Kibana-Logmanager State
  * Unit RSyslog-Logmanager State
  * Unit Elastic Blockchain Proxy State
  * Unit Logstash State
  * Unit Filebeat State
* `nx-ss-neteye-endpoint-elastic-state`: Service Set providing common monitoring for Elasticsearch Service on NetEye
  * Unit Elasticsearch State
  * Disk Elasticsearch Free space
  * Elastic CPU Usage
  * Elastic Memory Usage
  * Elastic Disk Usage
  * Elastic Disk Watermark Flooding
* `nx-ss-neteye-endpoint-filebeat-state`: Service Set providing common monitoring for Filebeat Service on NetEye"
  * Filebeat Status
  * Unit Filebeat State
* `nx-ss-neteye-endpoint-logstash-state`: Service Set providing common monitoring for Logstash Service on NetEye
  * Logstash Status
  * Logstash Total EPS
  * Unit Logstash State
* `nx-ss-neteye-endpoint-elastic-agent-state`: Service Set providing common monitoring for Elastic Agent Service on NetEye
  * Elastic Agent Status
  * Unit Elastic Agent State
* `nx-ss-neteye-endpoint-elastic-agent-extras`: Service Set providing extra monitoring for Elastic Agent Service on NetEye
  * Elasticsearch Current ESTABLISHED Connection
  * Elastic Agent Total EPS
* `nx-ss-neteye-local-siem`: All checks needed for SIEM module on NetEye Local
  * Fleet Agent Status
  * Endpoint Agent Status
  * Elastic SLM Policy
  * Elastic SLM Status
* `nx-ss-neteye-local-extras`: All checks extras on NetEye Local
  * Elastic Dataset Status - neteye_system_internal


#### Command


The following Commands can be used to freely create Command Objects, Command Template and Service Template. 

_Remember to not edit these Service Templates as they will be restored/updated at the next NEP Package update_:

* `nx-c-check-elastic-slm-policy`
* `nx-c-check-elastic-slm-status`
* `nx-c-check-es-system`
* `nx-c-check-filebeat`
* `nx-c-check-logstash`
* `nx-c-check-logstash-events`
* `nx-c-check-logstash-queue`
* `nx-c-check-kibana-stats`
* `nx-c-check_elastic_ingest_status`
* `nx-c-check-elastic-agent-status`
* `nx-c-check-elastic-endpoint-status`
* `nx-c-fleet-agent-status`
* `nx-c-endpoint-agent-status`
* `nx-c-check_elastic_dataset_status`
* `nx-c-check_elastic_transforms_status`
* `nx-c-check_elastic_disk_watermark_flooding`


#### Notification

This NEP doesn't provide any Notification definition


### Automation

This NEP doesn't provide any Automation


### Tornado Rules

The following Tornado rule are provided:

* ElasticAgent_Beats_Logstash: Creates a Service Object that checks how many events for a specific Elasticsearch Dataset are found during the defined timestamp
* Elastic_Transforms_OK: Creates a Service Object that reflect the status OK for a specific Elasticsearch Transform
* Elastic_Transforms_Warning: Creates a Service Object that reflect the status Warning for a specific Elasticsearch Transform
* Elastic_Transforms_Critical: Creates a Service Object that reflect the status Critical for a specific Elasticsearch Transform
* Elastic_Transforms_Unknown: Creates a Service Object that reflect the status Unknown for a specific Elasticsearch Transform
* elproxy_verification: Creates a Service Object that performs Blockchain Verification
* elproxy_duplicates_removal: Creates a Service Objects that is linked to the Service Object created with the `elproxy_verification` rule, and performs removal of duplicated documents inside the Blockchain

**Please Note**: 

1. The Webhook created for the EBP Verification is autogenerated, and you will need to manually change the secret token on `/etc/neteye-dpo` with the content of `/root/.pwd_webhook_nx_elproxy_verification`.
2. All Dockers Blockchain Verification configurations under `/etc/neteye-dpo` must have the same value for `webhook_token` since Tornado does not support multiple webhook tokens for the same webhook id.


### Dashboard ITOA

This NEP doesn't provide any ITOA Dashboards


### Metrics

This NEP doesn't generate any Performance Data from its commands


## Usage


### Examples

![Services Sample Director](doc-images/sample-elastic-services-director.png)

#### Using a host template provided by the NEP

No Host Template provided by this NEP


#### Using a service template provided by the NEP

Example of Service Template `nx-ss-neteye-siem-services-state`:

![Service Set Example](doc-images/service-set-example.png)


