#!/usr/bin/env bash
###########################################################
## Retrive all ElasticAgent status from Kibana Fleet API ##
###########################################################

DEFAULT_FILE="/neteye/shared/icinga2/data/lib/icinga2/elastic-agent_status.json"

print_help() {
    echo ""
    echo "This script retrive ElasticAgent status by Kibana Fleet API and write to Json File"
    echo ""
    echo "Usage:"
    echo "-h"
    echo "-f <file_path>    [optional]    ... file path of JSON result of Fleet API (default: $DEFAULT_FILE)"
    exit 0
}

# --- Read options
while getopts "hf:" opt; do
    case "${opt}" in
        f)
            JSON_FILE=${OPTARG}
            ;;
        h)
            print_help
            ;;
        *)
            print_help
            ;;
    esac
done

if [ -z $JSON_FILE ]; then
    JSON_FILE=$DEFAULT_FILE
fi

####### MAIN #############
KBN_USER="kibana_monitoring"
KBN_PASSWORD="@@PASSWORD@@"
PAGE=1
PER_PAGE=200
TOTAL_HOSTS=0
ALL_AGENTS="[]"

while true; do
    KBN_RESPONSE=$(/usr/bin/curl -u "${KBN_USER}:${KBN_PASSWORD}" -XGET -H 'kbn-xsrf: true' "http://kibana.neteyelocal:5601/api/fleet/agents?perPage=$PER_PAGE&page=$PAGE" -sS -w  '{"ErrorCode": %{http_code}}')

    KBN_CURL_RESULT="$(echo "$KBN_RESPONSE"  | jq 'select(.ErrorCode !=null).ErrorCode')"

    if [[ "$KBN_CURL_RESULT" != "200" ]];then
        echo "Error on Kibana curl"
        echo $KBN_RESPONSE
        exit 2
    else
        # Extract agents from the response
        AGENTS=$(echo "$KBN_RESPONSE" | jq -r 'select(.ErrorCode == null) | .list')
        ALL_AGENTS=$(echo $ALL_AGENTS $AGENTS | jq -s 'add')

        # Get the total number of agents from the first response
        if [ $PAGE -eq 1 ]; then
            TOTAL_HOSTS=$(echo "$KBN_RESPONSE" | jq -r '.total' | grep -oE '^[0-9]+' )
        fi

        # Calculate the number of pages
        PAGES=$(echo $(( ($TOTAL_HOSTS + $PER_PAGE - 1) / $PER_PAGE  )) | bc)

        # If we have fetched all pages, exit by breaking the loop
        if [ $PAGE -ge $PAGES ]; then
            break
        fi

        # Increase pagination to iterate on the following page
        PAGE=$((PAGE + 1))
    fi
done

# Write the aggregated result to the file
echo "$ALL_AGENTS" | jq '.' > $JSON_FILE
echo "Exported $TOTAL_HOSTS host(s) from Kibana Fleet Management.|hosts=$TOTAL_HOSTS;;;0;"