#! /bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh


SERVICE="icinga2-master"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

echo 'Getting NetEye Corporate IPs'
# Get the main IP of NetEye
# On a Cluster, it is the VIP from PCS
# On a single node, function get_neteye_ip returns 127.0.0.1, so the IP must be found "manually"
neteye_ips=$(get_neteye_ip)
if [ "${neteye_ips}" == "127.0.0.1" ]; then
    # In case of a single node, must lookup for the host fqdn on /etc/hosts file
    # assumptions: hostname of the host is the real FQDN on NetEye and its IP is stored into /etc/hosts
    neteye_ips=$(grep $(hostname) /etc/hosts | awk '{ print $1 }')
fi

# In a cluster environment, loop into /etc/neteye-cluster to find the extenal name of each cluster node,
# then resolves all names into IPs using /etc/hosts file
if [ -f /etc/neteye-cluster ]; then
    for node in $(cat /etc/neteye-cluster | jq '.Nodes[].hostname_ext')
    do
            node_ip=$(eval grep $node /etc/hosts | awk '{ print $1 }')
            neteye_ips="${neteye_ips},${node_ip}"
    done
fi

echo ${neteye_ips}

echo "Updating NRPE Config files for agents"
sed -i "s/<NETEYE_IPS>/${neteye_ips}/g" /neteye/shared/icinga2/data/nrpe/nsclient.ini
sed -i "s/<NETEYE_IPS>/${neteye_ips}/g" /neteye/shared/icinga2/data/nrpe/nrpe.cfg
