#!/bin/bash
## Install required RPMS for check_nrpe to work:
## compat-openssl10 and libnsl

echo "Installing Compat-OpenSSL 10 and LibNSL"
dnf install -y compat-openssl10 libnsl
