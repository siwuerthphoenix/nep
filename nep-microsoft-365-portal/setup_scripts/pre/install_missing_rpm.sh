#!/bin/bash
## Install xmltodict python3 library

. /usr/share/neteye/scripts/rpm-functions.sh

dnf install python3-xmltodict -y --enablerepo=epel
