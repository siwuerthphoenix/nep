#!/bin/bash
## 

. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo "Adding VCSA file on Icinga2 Master"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo " Not a cluster node. Skipping."
    exit 0
fi

if [ -f "/neteye/shared/icinga2/conf/vmware-auth-files/generic-vcsa" ]; then
    echo "VCSA file already exist."
    exit 0
fi

mkdir -p /neteye/shared/icinga2/conf/vmware-auth-files/
touch /neteye/shared/icinga2/conf/vmware-auth-files/generic-vcsa
chown -R root:icinga /neteye/shared/icinga2/conf/vmware-auth-files

cat << EOF > /neteye/shared/icinga2/conf/vmware-auth-files/generic-vcsa
username=XXXX@
password=
EOF

chmod 640 /neteye/shared/icinga2/conf/vmware-auth-files/generic-vcsa
