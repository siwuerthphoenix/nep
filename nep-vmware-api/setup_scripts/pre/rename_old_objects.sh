. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi
 

declare -A command_objects
command_objects["nx-c-check_vmware_snapshot"]="nx-c-check-vmware-snapshot"
command_objects["nx-c-check_vmware_api"]="nx-c-check-vmware-api"
command_objects["nx-c-check_vmware_api_datacenter"]="nx-c-check-vmware-api-datacenter"


echo "Removing legacy Director Objects"

## Rename command objects
for c in "${!command_objects[@]}"; do
    tmp=$(icingacli director command exist "$c")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director command set "$c" --object_name "${command_objects[$c]}"
    fi
done

declare -A host_objects
host_objects["nx-ht-vmware-host"]="nx-ht-vmware-api-host-system"
host_objects["nx-ht-vmware-vm"]="nx-ht-vmware-api-virtual-machine"
host_objects["nx-ht-vmware-vcenter"]="nx-ht-vmware-api-vcsa"


echo "Removing legacy Director Objects"

## Rename host objects
for s in "${!host_objects[@]}"; do
    tmp=$(icingacli director host exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        echo " - Removing legacy object ${s}"
        icingacli director host set "$s" --object_name "${host_objects[$s]}"
    fi
done

