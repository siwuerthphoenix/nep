Base NEP for Microsoft Exchange Server

It is assumed that the Icinga2 Agent is installed and usable. Copy the Powershell Plugins to the C:/scripts/neteye/ on your Exchange Server.

The following roles will be added to nx_exchange_roles_list:
- Client Access Server (cas)
- Mailbox Server (mailbox)