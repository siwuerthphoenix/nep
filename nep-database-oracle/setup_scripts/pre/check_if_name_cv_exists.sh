#!/bin/bash

#Custom Variable name
cv_name="name"

#Default output message and dummy_host state
output_message="UNKNOWN: something went wrong"
exit_state=2

#TODO
cv_count=0

for table in "icinga_command_var" "icinga_host_var" "icinga_notification_var" "icinga_service_var" "icinga_service_set_var" "icinga_user_var" "icinga_var"
do
    get_cv_count="SELECT COUNT(*) AS count from $table where varname='$cv_name'"
    result=$(mysql 'director' --execute "$get_cv_count" | grep -v count)
    cv_count=$((cv_count + result))
done

if [ "$cv_count" -gt "0" ]; then
    output_message="CRITICAL: $cv_name exists"
    exit_state=1
fi

if [ "$cv_count" -eq "0" ]; then
    output_message="OK: $cv_name does not exist"
    exit_state=0
fi

echo $output_message
exit $exit_state