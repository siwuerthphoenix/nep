#!/bin/bash

#Custom Variable name
old_cv_name="nx_oracle_health_tablespace_name"
new_cv_name="nx_oracle_health_name"

#Default output message and dummy_host state
# output_message="UNKNOWN: something went wrong"
# exit_state=0

#TODO
for table in "icinga_command_var" "icinga_host_var" "icinga_notification_var" "icinga_service_var" "icinga_service_set_var" "icinga_user_var"
do
    update_cv_name="UPDATE $table SET varname='$new_cv_name' WHERE varname='$old_cv_name'"
    result=$(mysql 'director' --execute "$update_cv_name")
    result_status=$?
    if [ "$result_status" -gt "0" ]; then
        output_message="ERROR during $update_cv_name"
        exit $result_status
    fi
done

exit $?

# echo $output_message
# exit $exit_state