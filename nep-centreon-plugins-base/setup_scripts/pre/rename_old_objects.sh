. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

declare -A command_objects
command_objects["nx-c-centreon-plugins-snmp"]="nx-ct-centreon-plugin-snmp"
command_objects["nx-c-centreon-plugins-restapi"]="nx-ct-centreon-plugin-restapi"
command_objects["nx-c-centreon-plugins-ssh"]="nx-ct-centreon-plugin-ssh"
command_objects["nx-ct-centreon-plugins-snmp"]="nx-ct-centreon-plugin-snmp"
command_objects["nx-ct-centreon-plugins-restapi"]="nx-ct-centreon-plugin-restapi"
command_objects["nx-ct-centreon-plugins-ssh"]="nx-ct-centreon-plugin-ssh"


echo "Removing legacy Director Objects"

## Rename command set objects
for s in "${!command_objects[@]}"; do
    tmp=$(icingacli director command exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        echo " - Rename legacy object ${s} to ${command_objects[$s]}"
        icingacli director command set "$s" --object_name "${command_objects[$s]}"
    fi
done