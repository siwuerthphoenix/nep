#!/bin/bash
# Install Centreon Repo's definitions version 23.04

# From version 23.04, repo definitions are no more shipper with package centreon-release.
# In fact, centreon-release is no more. Must use the official procedure reported at the following URL:
#   https://docs.centreon.com/docs/installation/installation-of-a-poller/using-packages/#install-the-repositories

. /usr/share/neteye/scripts/rpm-functions.sh

function create_folder_for_temp_files() {
    if [ ! -d "/neteye/shared/icinga2/data/lib/centreon-plugins/" ]; then
        mkdir /neteye/shared/icinga2/data/lib/centreon-plugins/
        chown icinga:icingaweb2 /neteye/shared/icinga2/data/lib/centreon-plugins/
    fi 
}

# If installed, remove rpm centreon-release
echo 'Checking if Centreon Repo definition is shipped via RPM...'
if rpm -q centreon-release; then
    echo 'Removing old repo definitions'
    dnf -y remove centreon-release
fi

# Add new repo definitions
echo 'Adding Centreon Repo definitions using DNF Config Manager'
dnf config-manager --add-repo https://packages.centreon.com/rpm-standard/23.04/el8/centreon-23.04.repo
if [ $? -eq 0 ]; then
    echo ' Done'
else
    echo ' Unable to add Centreon Repo definitions'
    exit 1
fi

# Disable all Centreon Repos, to avoid future issues with dnf update
# If disabling fails, the script terminates with an error
echo 'Disabling repositories...'
for repo in $(dnf repolist --enabled | grep -e '^centreon' | awk '{ print $1 }'); do
	echo -n "Disabling repo $repo..."
	dnf config-manager --disable $repo > /dev/null
	if [ $? -eq 0 ]; then
        echo ' Done'
    else
        echo ' Failed'
        exit 1
    fi
done

# Preparing folders for temp files
echo 'Creating directory for temporary files'
if is_cluster ;then
    # In case of cluster architecture, just run it there icinga2-master is running
    if [ is_active "icinga2-master" ]; then
        create_folder_for_temp_files
    fi
else
    # On a single node, just run it
    create_folder_for_temp_files
fi

echo 'Done'
