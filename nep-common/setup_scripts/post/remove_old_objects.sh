
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

host_objects=( "nx-ht-client-agent" "nx-ht-server-agent" )

## Delete host objects
for s in "${host_objects[@]}"; do
    tmp=$(icingacli director host exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        ## Delete service set objects
        icingacli director host delete "$s"
    fi
done

 