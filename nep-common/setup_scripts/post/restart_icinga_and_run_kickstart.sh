. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo "Restarting Icinga2 Local instance (if active)"
systemctl is-active icinga2 && systemctl restart icinga2

echo "Restarting Icinga2 Shared instance"
SERVICE="icinga2-master"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

systemctl restart icinga2-master

echo "Running Director Kickstart Wizard"
icingacli director kickstart run