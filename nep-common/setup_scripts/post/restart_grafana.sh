. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo "Restarting Grafana"

SERVICE="grafana"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

if is_cluster; then
    echo " - Performing cluster-controlled restart of Grafana"
    pcs resource restart grafana
else
    echo " - Restarting unit grafana-server.service"
    systemctl restart grafana-server
fi
