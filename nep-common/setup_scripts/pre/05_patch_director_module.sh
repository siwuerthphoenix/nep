#!/bin/bash
## Add patch from Director 1.11 

pushd /usr/share/icingaweb2/modules/director/

echo "[i] Installing patch for Director 1.10.2"

result=($(md5sum library/Director/Objects/IcingaServiceSet.php))
if [[ $result == "be6576c8984743a2b94643dd00322ee3" ]];then
    cat << 'EOF' | patch -p1
From 51380a9e0b75e1e76ed9431a01e46e1b1cc4b155 Mon Sep 17 00:00:00 2001
From: Thomas Gelf <thomas@gelf.net>
Date: Tue, 15 Nov 2022 17:28:33 +0100
Subject: [PATCH] IcingaServiceSet: respect indivicual template zone

fixes #1589
fixes #2356
---
 library/Director/Objects/IcingaServiceSet.php | 27 +++++++++++++------
 1 files changed, 22 insertions(+), 8 deletions(-)

diff --git a/library/Director/Objects/IcingaServiceSet.php b/library/Director/Objects/IcingaServiceSet.php
index 8217a5947..a435d1a32 100644
--- a/library/Director/Objects/IcingaServiceSet.php
+++ b/library/Director/Objects/IcingaServiceSet.php
@@ -295,8 +295,8 @@ public function onDelete()
      */
     public function renderToConfig(IcingaConfig $config)
     {
-        // always print the header, so you have minimal info present
-        $file = $this->getConfigFileWithHeader($config);
+        $files = [];
+        $zone = $this->getRenderingZone($config) ;

         if ($this->get('assign_filter') === null && $this->isTemplate()) {
             return;
@@ -334,8 +334,15 @@ public function renderToConfig(IcingaConfig $config)
             }

             $this->copyVarsToService($service);
+            $zone = $service->getRenderingZone($config);
+            $file = $this->getConfigFileWithHeader($config, $zone, $files);
             $file->addObject($service);
         }
+
+        if (empty($files)) {
+            // always print the header, so you have minimal info present
+            $this->getConfigFileWithHeader($config, $zone, $files);
+        }
     }

     /**
@@ -355,14 +362,18 @@ public function getBlacklistedHostnames($service)
         return $lookup->getBlacklistedHostnamesForService($service);
     }

-    protected function getConfigFileWithHeader(IcingaConfig $config)
+    protected function getConfigFileWithHeader(IcingaConfig $config, $zone, &$files = [])
     {
-        $file = $config->configFile(
-            'zones.d/' . $this->getRenderingZone($config) . '/servicesets'
-        );
+        if (!isset($files[$zone])) {
+            $file = $config->configFile(
+                'zones.d/' . $zone . '/servicesets'
+            );
+
+            $file->addContent($this->getConfigHeaderComment($config));
+            $files[$zone] = $file;
+        }

-        $file->addContent($this->getConfigHeaderComment($config));
-        return $file;
+        return $files[$zone];
     }

     protected function getConfigHeaderComment(IcingaConfig $config)
EOF
    echo " - File IcingaServiceSet.php has been patched"
else
    echo " - File IcingaServiceSet.php already patched"
fi

result=($(md5sum library/Director/Objects/IcingaArguments.php))
if [[ $result == "dc392845d1382c629632f5cbb5e10fa0" ]];then
    cat << 'EOF' | patch -p1
diff --git a/library/Director/Objects/IcingaArguments.php b/library/Director/Objects/IcingaArguments.php
index e4c23e02..22bf9142 100644
--- a/library/Director/Objects/IcingaArguments.php
+++ b/library/Director/Objects/IcingaArguments.php
@@ -155,7 +155,9 @@ class IcingaArguments implements Iterator, Countable, IcingaConfigRenderer
             if (property_exists($value, 'type')) {
                 // argument is directly set as function, no further properties
                 if ($value->type === 'Function') {
-                    $attrs['argument_value'] = self::COMMENT_DSL_UNSUPPORTED;
+                    $attrs['argument_value'] = property_exists($value, 'body')
+                        ? $value->body
+                        : self::COMMENT_DSL_UNSUPPORTED;
                     $attrs['argument_format'] = 'expression';
                 }
             } elseif (property_exists($value, 'value')) {
EOF
    echo " - File IcingaArguments.php has been patched"
else
    echo " - File IcingaArguments.php already patched"
fi

result=($(md5sum library/Director/Objects/DirectorJob.php))
if [[ $result == "da0d1cc8c0083ee5bdb40bebc10d447b" ]];then
    cat << 'EOF' | patch library/Director/Objects/DirectorJob.php
--- library/Director/Objects/DirectorJob.php     2023-08-11 09:36:24.000000000 +0200
+++ library/Director/Objects/DirectorJob.php.patched     2023-11-10 11:57:26.093640838 +0100
@@ -231,25 +231,22 @@
         $keyCol = $dummy->keyName;
         $properties = (array) $plain;
         if (isset($properties['originalId'])) {
-            $id = $properties['originalId'];
             unset($properties['originalId']);
-        } else {
-            $id = null;
         }
+
         $name = $properties[$keyCol];

-        if ($replace && $id && static::existsWithNameAndId($name, $id, $db)) {
-            $object = static::loadWithAutoIncId($id, $db);
-        } elseif ($replace && static::exists($name, $db)) {
+        if ($replace && static::exists($name, $db)) {
             $object = static::load($name, $db);
         } elseif (static::exists($name, $db)) {
             throw new DuplicateKeyException(
-                'Director Job "%s" already exists',
+                'Director Job %s already exists',
                 $name
             );
         } else {
             $object = static::create([], $db);
         }
+        $object->setProperties($properties);

         $settings = (array) $properties['settings'];

@@ -267,9 +264,6 @@

         $properties['settings'] = (object) $settings;
         $object->setProperties($properties);
-        if ($id !== null) {
-            $object->reallySet($idCol, $id);
-        }

         return $object;
     }
EOF
    echo " - File IcingaArguments.php has been patched"
else
    echo " - File IcingaArguments.php already patched"
fi
popd