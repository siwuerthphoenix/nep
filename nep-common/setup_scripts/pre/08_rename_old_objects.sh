. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

declare -A service_objects
service_objects["nx-st-agent-linux-init-state"]="nx-st-agent-linux-initd-service-state"
service_objects["nx-st-agent-linux-unit-state"]="nx-st-agent-linux-systemd-unit-state"
service_objects["nx-st-agent-linux-disk-space-usage"]="nx-st-agent-linux-disk-free-space"
service_objects["nx-st-agent-linux-disk-freespace"]="nx-st-agent-linux-disk-free-space"
service_objects["nx-st-agent-windows-disk-space-usage"]="nx-st-agent-windows-disk-free-space"

echo "[i] Removing legacy Director Objects"

## Rename service set objects
for s in "${!service_objects[@]}"; do
    tmp=$(icingacli director service exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        echo " - Removing legacy object ${s}"
        icingacli director service set "$s" --object_name "${service_objects[$s]}"
    fi
done