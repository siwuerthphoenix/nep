. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

# Enables businessprocess module on Icingaweb 2
echo "[i] Enabling Icingaweb2 Module BusinessProcess"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

icingacli module enable businessprocess