#!/bin/bash
## Enable fileshipper 

. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

base_nep="/usr/share/neteye/nep/"
FILE="/neteye/shared/icingaweb2/conf/modules/fileshipper/imports.ini"

# Enables fileshipper module on Icingaweb 2
echo "[i] Enabling Icingaweb2 Module FileShipper"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

icingacli module enable fileshipper

if [ ! -f "$FILE" ]; then
    echo " - No configurfation for module FileShipper found. Applying default settings."
    touch $FILE
fi

if grep -qxF '[NX NEP Private Data]' $FILE ; then
    echo " - NEP Data file source already exist, nothing to do."
else
    ## Create NEP Data import definition in FileShipper
    echo " - Adding NEP Data file source to FileShipper configuration"
    cat << EOF >> $FILE

[NX NEP Private Data]
basedir="/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/"

EOF

    ## Fix ownership of folder and files
    chown -R root:icingaweb2 /neteye/shared/icingaweb2/conf/modules/fileshipper/

fi