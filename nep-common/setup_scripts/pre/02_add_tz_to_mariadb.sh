#! /bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

# In case of cluster deployment, ensures this script runs only on the main node
SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

tz_count=$(mysql -BNe 'SELECT COUNT(*) FROM mysql.time_zone_name;')
if [ "${tz_count}" -eq "0" ]; then
	echo "No Timezones defined on MariaDB. Importing default definitions..."
	mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql mysql
fi