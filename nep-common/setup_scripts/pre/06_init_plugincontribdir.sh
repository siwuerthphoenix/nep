. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

plugin_contrib_dir="/neteye/shared/monitoring/plugins"

function update_plugin_contrib_dir {
    config_file="/neteye/$1/icinga2/conf/icinga2/constants.conf"

    sed -i 's|\s*const\s\s*PluginContribDir\s\s*=\s\s*"\s*"|const PluginContribDir = "'${plugin_contrib_dir}'"|g' ${config_file}
    # sed -i '/const PluginContribDir= \"\"/c const PluginContribDir= \"'${plugin_contrib_dir}'\"' /neteye/local/icinga2/conf/icinga2/constants.conf
}

# Updating PluginContribDir on Icinga2 Local instance
echo "[i] Updating PluginContribDir definition"

echo " - Updating Local Icinga2 instance"
update_plugin_contrib_dir "local"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    exit 0
fi

echo " - Updating Shared Icinga2 instance"
update_plugin_contrib_dir "shared"
