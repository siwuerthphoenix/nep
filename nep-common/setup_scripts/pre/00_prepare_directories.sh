#! /bin.bash

# Create directory if not exists
function create_directory() {
    directory_path="$1"
    directory_description="$2"

    if [ -d ${directory_path} ] ; then
        echo " - Directory \"${directory_description}\" already exists"
    else
        echo " - Creating directory \"${directory_description}\""
        mkdir -p ${directory_path}
    fi
}

# Ensures all the additional non-managed directories are presents
echo '[i] Creating directories...'
create_directory "/neteye/shared/icingaweb2/conf/modules/fileshipper/"              "Fileshipper configuration directory"
create_directory "/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/" "Fileshipper file repository for NEP"
create_directory "/neteye/shared/monitoring/plugins"                                "Plugin Contribution Directory"
create_directory "/usr/share/icingaweb2/public/img/nep"                                "Main Icons folder for NEP templates"