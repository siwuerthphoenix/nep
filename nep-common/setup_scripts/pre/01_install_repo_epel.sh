#!/bin/bash
## Install and disabel EPEL Repo

. /usr/share/neteye/scripts/rpm-functions.sh


echo "[i] Installing EPEL Repo definitions"
dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
if [ $? -eq 0 ]; then
    echo ' Done'
else
    echo ' Failed'
    exit 1
fi

echo "[i] Disabling EPEL Repos enabled by default"
for repo in $(dnf repolist --enabled | grep -e '^epel' | awk '{ print $1 }'); do
    echo -n "  Disabling repo $repo..."
    dnf config-manager --disable $repo > /dev/null
    if [ $? -eq 0 ]; then
        echo ' Done'
    else
        echo ' Failed'
        exit 1
    fi
done

