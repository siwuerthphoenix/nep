#Install Grafana Clock Panel plugin
echo "Installing Grafana Plugins"

GRAFANA_PLUGIN_DIR=/var/lib/grafana/plugins/
PLUGIN_PATH=/tmp/grafana-plugin.zip

echo "[i] Downloading Grafana Plugin"
if [ -f ${PLUGIN_PATH} ]; then rm -f ${PLUGIN_PATH}; fi
wget -O ${PLUGIN_PATH} https://grafana.com/api/plugins/grafana-clock-panel/versions/2.1.1/download

echo "[i] Unpaking Grafana Plugin"
unzip -o ${PLUGIN_PATH} -d ${GRAFANA_PLUGIN_DIR}/

echo "[i] Removing temporary files"
rm -f ${PLUGIN_PATH}

# Restart of grafana-server is required.
# It will be done in a post-setup step.