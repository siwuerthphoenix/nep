<?php

use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierCoalesce;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierCompareFieldContentAgainstPattern;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierConstantValue;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierCopy;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierNxArrayElementByPosition;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierNxGetArrayOfPropertyNames;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierNxMap;
use Icinga\Module\Nep\ProvidedHook\Director\PropertyModifier\PropertyModifierRejectOnPatternMatch;

$this->provideHook('director/PropertyModifier', PropertyModifierCoalesce::class);
$this->provideHook('director/PropertyModifier', PropertyModifierCompareFieldContentAgainstPattern::class);
$this->provideHook('director/PropertyModifier', PropertyModifierConstantValue::class);
$this->provideHook('director/PropertyModifier', PropertyModifierCopy::class);
$this->provideHook('director/PropertyModifier', PropertyModifierNxArrayElementByPosition::class);
$this->provideHook('director/PropertyModifier', PropertyModifierNxGetArrayOfPropertyNames::class);
$this->provideHook('director/PropertyModifier', PropertyModifierNxMap::class);
$this->provideHook('director/PropertyModifier', PropertyModifierRejectOnPatternMatch::class);
