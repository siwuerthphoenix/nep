<?php

namespace Icinga\Module\Nep\Clicommands;

use Icinga\Module\Director\Cli\ObjectsCommand;

/**
 * Manage Icinga Endpoints
 *
 * Use this command to list Icinga Endpoint objects
 */
class EndpointsCommand extends ObjectsCommand
{
}
