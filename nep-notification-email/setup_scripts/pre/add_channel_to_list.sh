#!/bin/bash
## Add channel to list
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

key="text-email"
name="E-Mail (text)"
FILE="/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/nx-channel-list.csv"

if grep -q "^${key}," $FILE ; then
    echo "Channel '$key' already present... Nothing to do."
else
cat << EOF >> $FILE
$key,$name,string,null
EOF

fi

key="html-email"
name="E-Mail (HTML)"
FILE="/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/nx-channel-list.csv"

if grep -q "^${key}," $FILE ; then
    echo "Channel '$key' already present... Nothing to do."
else
cat << EOF >> $FILE
$key,$name,string,null
EOF

fi
