#!/bin/bash
## Add default config for HTML email
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icinga2-master"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi


FILE="/neteye/shared/icinga2/conf/icinga2/scripts/mail-html-notification.cfg"


if [ -f $FILE ]; then
    echo "Config file '$FILE' already present... Nothing to do."
else
cat << EOF >> $FILE
# This is the logo image file, the path must point to a valid JPG, GIF or PNG file, i.e.
# the Monitor logo or your company logo or whatelse. Best size is rectangular up to 160x80px.
# example: /var/www/html/images/our_company_logo-112x46.png

our \$logofile = "/usr/share/icingaweb2/public/img/neteye/neteye-logo.png";

# SMTP related data: If the commandline argument -H/--smtphost was not
# given, we use the provided value in \$smtphost below as the default.
# If the mailserver requires auth, an example is further down the code.

our \$smtphost = "127.0.0.1";

# Here I define the HTML color values for each Nagios notification type.
# The color values are used for highlighting the background of the
# notification type cell.

our %NOTIFICATIONCOLOR=('PROBLEM'=>'#FF8080',
                       'PROBLEM_WARN'=>'#FFFF80',
                       'RECOVERY'=>'#80FF80',
                       'ACKNOWLEDGEMENT'=>'#FFFF80',
                       'DOWNTIMESTART'=>'#80FFFF',
                       'DOWNTIMEEND'=>'#80FF80',
                       'DOWNTIMECANCELLED'=>'#FFFF80',
                       'FLAPPINGSTART'=>'#FF8080',
                       'FLAPPINGSTOP'=>'#80FF80',
                       'FLAPPINGDISABLED'=>'#FFFF80',
                       'CRITICAL'=>'#FFEBEB',
                       'WARNING'=>'#FFFFC0',
                       'OK'=>'#C0FFC0',
                       'UNKNOWN'=>'#FFDDBB',
                       'UP'=>'#C0FFC0',
                       'DOWN'=>'#FFEBEB',
                       'UNREACHABLE'=>'#FFDDBB');


our \$tablelabel = "#000000";

EOF

fi
