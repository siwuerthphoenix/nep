. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

channel="text-email"
echo "Create User Template for '$channel' channel"

## Check exist and create
tmp=$(icingacli director user exist "nx-ut-$channel")
if [[ $tmp == *"does not"* ]]; then
    echo " - Add user template 'nx-ut-$channel'"
    icingacli director user create "nx-ut-$channel" --object_type template \
        --enable_notifications \
        --states "Critical" --states  "Down" --states "OK" --states "Up" --states "Warning" --states "Unknown" \
        --types "Custom" --types "Problem" --types "Recovery" \
        --types "Acknowledgement" --types "DowntimeStart" --types "DowntimeRemoved" --types "DowntimeEnd" \
        --types "FlappingStart" --types "FlappingEnd"
fi


