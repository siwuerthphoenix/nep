# Notification Email

The `nep-notification-email` adds to the Notification Facility provided with `nep-notification-base` the ability to send Notifications using Text Email.

# Table of Contents

1. [Prerequisites](#prerequisites)
2. [Installation](#installation)
3. [Packet Contents](#packet-contents)
4. [Usage](#usage)

## Prerequisites

This package can be installed on systems running the software described below. Systems with equivalent components are also suitable for installation.

| Sofware Version | Version |
| --- | ----------- |
| NetEye | 4.22+ |
| nep-common | 0.1.0+ |
| nep-notification-base | 0.0.3+ |

##### Required NetEye Modules

| NetEye Module |
| --- |
| Core |

## Installation

### NEP Installation

To setup Package `nep-notification-email`, just use the Setup Utility:

```
nep-setup install nep-notification-email
```
After the installation is complete, you can use objects and configure the NEP.

## Packet Contents

This `nep-notification-email` does not provide any Object for the End User. All Objects will be automatically created after the current Director Configuration is deployed.

### Director/Icinga Objects

The Package contains the following Director Objects.

| Object Type | Object Name | Editable | Containing File |
| ----------------------- | ----------- | -------- | --------------- |
| Icinga2 Notification Template | nx-nt-channel-text-email | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Template | nx-nt-channel-text-email-for-host | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Template | nx-nt-channel-text-email-for-service | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Host Apply Rule | nx-n-basic-text-email-to-users-from-host | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Host Apply Rule | nx-n-basic-text-email-to-groups-from-host | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Service Apply Rule | nx-n-basic-text-email-to-users-from-host | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Service Apply Rule | nx-n-basic-text-email-to-groups-from-hosts | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Service Apply Rule | nx-n-basic-text-email-to-users-from-service | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |
| Icinga2 Notification Service Apply Rule | nx-n-basic-text-email-to-groups-from-service | Yes | custom_files/neteye_shared_root/icinga2/conf.d/nx-notification-basic-text-email.conf |

#### Commands

| Icinga Command | File Path |
| ---------------- | ----------- |
| mail-host-notification | /neteye/shared/icinga2/conf/icinga2/scripts/mail-host-notification.sh |
| mail-service-notification | /neteye/shared/icinga2/conf/icinga2/scripts/mail-service-notification.sh |
| nx-c-mail-html-host-notification | /neteye/shared/icinga2/conf/icinga2/scripts/mail-html-notification.pl |
| nx-c-mail-html-service-notification | /neteye/shared/icinga2/conf/icinga2/scripts/mail-html-notification.pl |

#### User Template

| User Template name | Description |
| ---------------- | ----------- |
| nx-ut-html-email| Notification User Template used by Apply rules |