#!/bin/bash

. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo '[i] Renaming objects from previous versions of this NEP'

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

# Rename objects from an older setup
echo '[i] Renaming old custom variables and data fields...'
cat << 'EOF' | mysql director
update icinga_command_var set varname='nx_centreon_influxdb_ssl_opt'               where varname='nx_centreon_influx_ssl_opt';
update icinga_command_var set varname='nx_centreon_influxdb_timeout'               where varname='nx_centreon_plugin_influxdb_timeout';
update icinga_command_var set varname='nx_centreon_influxdb_ssl'                   where varname='nx_centreon_plugin_influxdb_ssl';
update icinga_command_var set varname='nx_centreon_influxdb_hostname'              where varname='nx_centreon_plugin_influxdb_hostname';
update icinga_command_var set varname='nx_centreon_influxdb_port'                  where varname='nx_centreon_plugin_influxdb_port';
update icinga_command_var set varname='nx_centreon_influxdb_curl_opt'              where varname='nx_centreon_plugin_influxdb_curl_opt';
update icinga_command_var set varname='nx_centreon_influxdb_backend'               where varname='nx_centreon_plugin_influxdb_backend';
update icinga_command_var set varname='nx_centreon_influxdb_password'              where varname='nx_centreon_plugin_influxdb_password';
update icinga_command_var set varname='nx_centreon_influxdb_proto'                 where varname='nx_centreon_plugin_influxdb_proto';
update icinga_command_var set varname='nx_centreon_influxdb_ssl_opt'               where varname='nx_centreon_plugin_influxdb_ssl_opt';
update icinga_command_var set varname='nx_centreon_influxdb_username'              where varname='nx_centreon_plugin_influxdb_username';
update icinga_command_var set varname='nx_centreon_influxdb_http_peer_addr'        where varname='nx_centreon_plugin_influxdb_http_peer_addr';
update icinga_command_var set varname='nx_centreon_influxdb_query_aggregation'     where varname='nx_centreon_plugin_influxdb_aggregation';
update icinga_command_var set varname='nx_centreon_influxdb_query_critical_status' where varname='nx_centreon_plugin_influxdb_critical_status';
update icinga_command_var set varname='nx_centreon_influxdb_query_instance'        where varname='nx_centreon_plugin_influxdb_instance';
update icinga_command_var set varname='nx_centreon_influxdb_query_multiple_output' where varname='nx_centreon_plugin_influxdb_multiple_output';
update icinga_command_var set varname='nx_centreon_influxdb_query_output'          where varname='nx_centreon_plugin_influxdb_output';
update icinga_command_var set varname='nx_centreon_influxdb_query_query'           where varname='nx_centreon_plugin_influxdb_query';
update icinga_command_var set varname='nx_centreon_influxdb_query_warning_status'  where varname='nx_centreon_plugin_influxdb_warning_status';

update icinga_service_var set varname='nx_centreon_influxdb_ssl_opt'               where varname='nx_centreon_influx_ssl_opt';
update icinga_service_var set varname='nx_centreon_influxdb_timeout'               where varname='nx_centreon_plugin_influxdb_timeout';
update icinga_service_var set varname='nx_centreon_influxdb_ssl'                   where varname='nx_centreon_plugin_influxdb_ssl';
update icinga_service_var set varname='nx_centreon_influxdb_hostname'              where varname='nx_centreon_plugin_influxdb_hostname';
update icinga_service_var set varname='nx_centreon_influxdb_port'                  where varname='nx_centreon_plugin_influxdb_port';
update icinga_service_var set varname='nx_centreon_influxdb_curl_opt'              where varname='nx_centreon_plugin_influxdb_curl_opt';
update icinga_service_var set varname='nx_centreon_influxdb_backend'               where varname='nx_centreon_plugin_influxdb_backend';
update icinga_service_var set varname='nx_centreon_influxdb_password'              where varname='nx_centreon_plugin_influxdb_password';
update icinga_service_var set varname='nx_centreon_influxdb_proto'                 where varname='nx_centreon_plugin_influxdb_proto';
update icinga_service_var set varname='nx_centreon_influxdb_ssl_opt'               where varname='nx_centreon_plugin_influxdb_ssl_opt';
update icinga_service_var set varname='nx_centreon_influxdb_username'              where varname='nx_centreon_plugin_influxdb_username';
update icinga_service_var set varname='nx_centreon_influxdb_http_peer_addr'        where varname='nx_centreon_plugin_influxdb_http_peer_addr';
update icinga_service_var set varname='nx_centreon_influxdb_query_aggregation'     where varname='nx_centreon_plugin_influxdb_aggregation';
update icinga_service_var set varname='nx_centreon_influxdb_query_critical_status' where varname='nx_centreon_plugin_influxdb_critical_status';
update icinga_service_var set varname='nx_centreon_influxdb_query_instance'        where varname='nx_centreon_plugin_influxdb_instance';
update icinga_service_var set varname='nx_centreon_influxdb_query_multiple_output' where varname='nx_centreon_plugin_influxdb_multiple_output';
update icinga_service_var set varname='nx_centreon_influxdb_query_output'          where varname='nx_centreon_plugin_influxdb_output';
update icinga_service_var set varname='nx_centreon_influxdb_query_query'           where varname='nx_centreon_plugin_influxdb_query';
update icinga_service_var set varname='nx_centreon_influxdb_query_warning_status'  where varname='nx_centreon_plugin_influxdb_warning_status';

update icinga_host_var set varname='nx_centreon_influxdb_ssl_opt'                  where varname='nx_centreon_influx_ssl_opt';
update icinga_host_var set varname='nx_centreon_influxdb_timeout'                  where varname='nx_centreon_plugin_influxdb_timeout';
update icinga_host_var set varname='nx_centreon_influxdb_ssl'                      where varname='nx_centreon_plugin_influxdb_ssl';
update icinga_host_var set varname='nx_centreon_influxdb_hostname'                 where varname='nx_centreon_plugin_influxdb_hostname';
update icinga_host_var set varname='nx_centreon_influxdb_port'                     where varname='nx_centreon_plugin_influxdb_port';
update icinga_host_var set varname='nx_centreon_influxdb_curl_opt'                 where varname='nx_centreon_plugin_influxdb_curl_opt';
update icinga_host_var set varname='nx_centreon_influxdb_backend'                  where varname='nx_centreon_plugin_influxdb_backend';
update icinga_host_var set varname='nx_centreon_influxdb_password'                 where varname='nx_centreon_plugin_influxdb_password';
update icinga_host_var set varname='nx_centreon_influxdb_proto'                    where varname='nx_centreon_plugin_influxdb_proto';
update icinga_host_var set varname='nx_centreon_influxdb_ssl_opt'                  where varname='nx_centreon_plugin_influxdb_ssl_opt';
update icinga_host_var set varname='nx_centreon_influxdb_username'                 where varname='nx_centreon_plugin_influxdb_username';
update icinga_host_var set varname='nx_centreon_influxdb_http_peer_addr'           where varname='nx_centreon_plugin_influxdb_http_peer_addr';
update icinga_host_var set varname='nx_centreon_influxdb_query_aggregation'        where varname='nx_centreon_plugin_influxdb_aggregation';
update icinga_host_var set varname='nx_centreon_influxdb_query_critical_status'    where varname='nx_centreon_plugin_influxdb_critical_status';
update icinga_host_var set varname='nx_centreon_influxdb_query_instance'           where varname='nx_centreon_plugin_influxdb_instance';
update icinga_host_var set varname='nx_centreon_influxdb_query_multiple_output'    where varname='nx_centreon_plugin_influxdb_multiple_output';
update icinga_host_var set varname='nx_centreon_influxdb_query_output'             where varname='nx_centreon_plugin_influxdb_output';
update icinga_host_var set varname='nx_centreon_influxdb_query_query'              where varname='nx_centreon_plugin_influxdb_query';
update icinga_host_var set varname='nx_centreon_influxdb_query_warning_status'     where varname='nx_centreon_plugin_influxdb_warning_status';

update director_datafield set varname='nx_centreon_influxdb_ssl_opt'               where varname='nx_centreon_influx_ssl_opt';
update director_datafield set varname='nx_centreon_influxdb_timeout'               where varname='nx_centreon_plugin_influxdb_timeout';
update director_datafield set varname='nx_centreon_influxdb_ssl'                   where varname='nx_centreon_plugin_influxdb_ssl';
update director_datafield set varname='nx_centreon_influxdb_hostname'              where varname='nx_centreon_plugin_influxdb_hostname';
update director_datafield set varname='nx_centreon_influxdb_port'                  where varname='nx_centreon_plugin_influxdb_port';
update director_datafield set varname='nx_centreon_influxdb_curl_opt'              where varname='nx_centreon_plugin_influxdb_curl_opt';
update director_datafield set varname='nx_centreon_influxdb_backend'               where varname='nx_centreon_plugin_influxdb_backend';
update director_datafield set varname='nx_centreon_influxdb_password'              where varname='nx_centreon_plugin_influxdb_password';
update director_datafield set varname='nx_centreon_influxdb_proto'                 where varname='nx_centreon_plugin_influxdb_proto';
update director_datafield set varname='nx_centreon_influxdb_ssl_opt'               where varname='nx_centreon_plugin_influxdb_ssl_opt';
update director_datafield set varname='nx_centreon_influxdb_username'              where varname='nx_centreon_plugin_influxdb_username';
update director_datafield set varname='nx_centreon_influxdb_http_peer_addr'        where varname='nx_centreon_plugin_influxdb_http_peer_addr';
update director_datafield set varname='nx_centreon_influxdb_query_aggregation'     where varname='nx_centreon_plugin_influxdb_aggregation';
update director_datafield set varname='nx_centreon_influxdb_query_critical_status' where varname='nx_centreon_plugin_influxdb_critical_status';
update director_datafield set varname='nx_centreon_influxdb_query_instance'        where varname='nx_centreon_plugin_influxdb_instance';
update director_datafield set varname='nx_centreon_influxdb_query_multiple_output' where varname='nx_centreon_plugin_influxdb_multiple_output';
update director_datafield set varname='nx_centreon_influxdb_query_output'          where varname='nx_centreon_plugin_influxdb_output';
update director_datafield set varname='nx_centreon_influxdb_query_query'           where varname='nx_centreon_plugin_influxdb_query';
update director_datafield set varname='nx_centreon_influxdb_query_warning_status'  where varname='nx_centreon_plugin_influxdb_warning_status';
EOF


declare -A command_objects
declare -A service_objects
command_objects["nx-ct-centreon-plugins-influxdb"]="nx-ct-centreon-influxdb"
command_objects["nx-c-centreon-plugins-influxdb-query"]="nx-c-centreon-influxdb-query"

service_objects["nx-st-agentless-centreon-plugin"]="nx-st-agentless-influxdb"
service_objects["nx-st-centreon-influxdb"]="nx-st-agentless-influxdb-centreon"
service_objects["nx-st-centreon-influxdb-query"]="nx-st-agentless-influxdb-centreon-query"


echo "[i] Renaming legacy Director Objects"

## Rename command set objects
for s in "${!command_objects[@]}"; do
    tmp=$(icingacli director command exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        echo " - Rename legacy object ${s} to ${command_objects[$s]}"
        icingacli director command set "$s" --object_name "${command_objects[$s]}"
    fi
done

## Rename service set objects
for s in "${!service_objects[@]}"; do
    tmp=$(icingacli director service exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        echo " - Rename legacy object ${s} to ${service_objects[$s]}"
        icingacli director service set "$s" --object_name "${service_objects[$s]}"
    fi
done
