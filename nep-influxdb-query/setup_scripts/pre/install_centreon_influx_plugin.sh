#!/bin/bash
# Install RPM for InfluxDB Centreon Plugin
# At present moment, the only working version is available from the unstable repo

echo "[i] Installing InfluxDB Centreon Plugin"
dnf -y --enablerepo=centreon-*-stable*,epel install centreon-plugin-Applications-Databases-Influxdb