. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

mysql_username='icingareadonly'

echo " - Add Database permissions for user "$mysql_username" access"
    cat << EOF | mysql
GRANT SELECT ON director.icinga_zone TO '${mysql_username}'@'localhost';
GRANT SELECT ON director.icinga_zone TO '${mysql_username}'@'%';
GRANT SELECT ON director.icinga_host TO '${mysql_username}'@'localhost';
GRANT SELECT ON director.icinga_host TO '${mysql_username}'@'%';
FLUSH PRIVILEGES;
EOF