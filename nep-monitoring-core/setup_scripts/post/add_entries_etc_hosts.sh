
. /usr/share/neteye/scripts/rpm-functions.sh

## Check and add if not exist service
get_or_append_service_etchost "icingaweb2"
get_or_append_service_etchost "snmptrapd"
get_or_append_service_etchost "neteye"

## check if module is installed before insert
if is_module_installed "neteye-siem"; then
    get_or_append_service_etchost "filebeat"
fi
