. /usr/share/neteye/scripts/rpm-functions.sh

monitoring_home=$(eval echo ~monitoring)
monitoring_authorized_keys_file=${monitoring_home}/.ssh/authorized_keys
icinga_home=$(eval echo ~icinga)
icinga_public_key_file=${icinga_home}/.ssh/id_rsa.pub

if [ ! -f ${authorized_keys_file} ]; then
    echo '[i] Creating authorized keys file for user monitoring'
    touch ${monitoring_authorized_keys_file}
    chmod 600 ${monitoring_authorized_keys_file}
    chown monitoring:monitoring ${monitoring_authorized_keys_file}
fi

if is_cluster ;then
    echo "[i] Get authorized_keys from all nodes"
    nodes=$(get_cluster_nodes_without_voting_hostname)
    for node in $nodes; do
        sed -i "/icinga@${node}$/d" ${monitoring_authorized_keys_file}
        ssh $node "cat ${icinga_public_key_file}" >> ${monitoring_authorized_keys_file}
    done
else
    echo "[i] Get authorized_keys on single-node"
    sed -i "/icinga@$(hostname)$/d" ${monitoring_authorized_keys_file}
    cat ${icinga_public_key_file} >> ${monitoring_authorized_keys_file}
fi


