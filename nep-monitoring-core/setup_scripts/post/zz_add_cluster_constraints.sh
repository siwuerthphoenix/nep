#! /bin/bash
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

# In case this is a cluster, ensures the required constraint are in place
if is_cluster && ! is_active "cluster_ip"
then
    echo "[i] Inactive cluster node. Skipping."
    exit 0
fi

# Check if constraint for tornado_icinga2_collector_group exists
pcs constraint | grep 'tornado_icinga2_collector_group with tornado_group'
if [ $? == 0 ]
then
    echo '[i] Constraint already present. Skipping.'
else
    echo '[i] Creating constraint for Tornado Icinga2 Collector'
    pcs constraint colocation add tornado_icinga2_collector_group with tornado_group
fi

exit 0