. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

echo "Creating MySQL Read Only user to Monitoring Core"

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
     echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

mysql_username='icingareadonly'
mysql_pwd_file=".pwd_$mysql_username"

mysql_host="mariadb.neteyelocal"
mysql_port=3306
FILE=/neteye/local/icinga2/data/spool/icinga2/.my.cnf


if [ -f "$mysql_pwd_file" ]; then
    echo "$mysql_pwd_file already exists. Skip."
else
    mysql_password=$(generate_and_save_pw "$mysql_username")
    echo " - Creating Database User for access"
    cat << EOF | mysql
CREATE USER IF NOT EXISTS '${mysql_username}'@'%' IDENTIFIED BY '${mysql_password}';
CREATE USER IF NOT EXISTS '${mysql_username}'@'localhost' IDENTIFIED BY '${mysql_password}';
ALTER USER '${mysql_username}'@'%' IDENTIFIED BY '${mysql_password}';
ALTER USER '${mysql_username}'@'localhost' IDENTIFIED BY '${mysql_password}';
FLUSH PRIVILEGES;
EOF

    echo " - Creating cnf file"
    cat << EOF > $FILE
[client]
host=mariadb.neteyelocal
user=${mysql_username}
password=${mysql_password}
EOF

    chown icinga:icinga $FILE

    if is_cluster ;then
        echo "[i] sync conf file on all nodes (exclude voting)"
        nodes=$(get_cluster_nodes_without_voting_hostname)

        for node in $nodes ; do rsync -avh $FILE $node:$FILE ; done
    fi

fi
