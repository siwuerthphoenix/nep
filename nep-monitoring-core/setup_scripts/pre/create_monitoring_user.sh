. /usr/share/neteye/scripts/rpm-functions.sh

# Install sudo
yum install sudo -y

if is_cluster && [ -f "/etc/sudoers.d/icinga" ]; then
        echo "Icinga sudoers already exists. Skip."
        exit 0
fi
        
icinga_home=$(eval echo ~icinga)
#Creating objects on all nodes
sudo -u icinga ssh-keygen -t rsa -N '' -f $icinga_home/.ssh/id_rsa <<< n
useradd monitoring
monitoring_home=$(eval echo ~monitoring)
install -d -m 700 -o monitoring -g monitoring $monitoring_home/.ssh

#Adding sudoers permissions for icinga on local node
echo 'icinga ALL = NOPASSWD: /usr/sbin/crm_mon' > /etc/sudoers.d/icinga

#sudores permissions 
chmod 644 /etc/sudoers.d/icinga
chown root:root /etc/sudoers.d/icinga
