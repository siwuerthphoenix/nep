. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh


SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi


## Remove Sync Rules
syncrule_objects=( "nx-sr-neteye-infrastructure-zones" "nx-sr-neteye-infrastructure-endpoints-update" "nx-sr-neteye-ip-duplicated-zones" "nx-sr-datalist-neteye-modules" )

for s in "${syncrule_objects[@]}"; do
    tmp=$(icingacli nep syncrule list | grep "$s")
    if [[ $tmp != "" ]]; then
        ## Import Source exist
        echo " - Removing Sync Rule object: ${s}"
        id=$(echo $tmp | awk '{ print $1 }')
        icingacli nep syncrule delete --id $id
    fi
done

## Remove Import Source
importsource_objects=( "nx-is-neteye-infrastructure-endpoints" "nx-is-neteye-infrastructure-zones" "nx-is-datalist-neteye-modules" )

for s in "${importsource_objects[@]}"; do
    tmp=$(icingacli nep importsource list | grep "$s")
    if [[ $tmp != "" ]]; then
        ## Import Source exist
        echo " - Removing Import Source object: ${s}"
        id=$(echo $tmp | awk '{ print $1 }')
        icingacli nep importsource delete --id $id
    fi
done
