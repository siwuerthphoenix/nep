. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

declare -A host_objects
host_objects["nx-ht-neteye-endpoint"]="nx-ht-neteye-mainsystem"
host_objects["nx-ht-neteye-endpoint-master"]="nx-ht-neteye-mainsystem-master"
host_objects["nx-ht-neteye-endpoint-master-cluster"]="nx-ht-neteye-mainsystem-cluster-node"
host_objects["nx-ht-neteye-endpoint-satellite"]="nx-ht-neteye-satellite"


echo "Removing legacy Director Objects"

## Rename host objects
for s in "${!host_objects[@]}"; do
    tmp=$(icingacli director host exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        echo " - Removing legacy object ${s}"
        icingacli director host set "$s" --object_name "${host_objects[$s]}"
    fi
done

