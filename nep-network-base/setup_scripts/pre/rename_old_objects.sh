. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

declare -A command_objects
command_objects["nx-c-check_nwc_health"]="nx-c-check-nwc-health"

declare -A host_objects
host_objects["nx-ht-snmp-network"]="nx-ht-network"
host_objects["nx-ht-snmp-network-load-balancer"]="nx-ht-network-load-balancer"
host_objects["nx-ht-snmp-network-wifi"]="nx-ht-network-wifi"
host_objects["nx-ht-snmp-network-voip"]="nx-ht-network-voip"
host_objects["nx-ht-snmp-network-firewall"]="nx-ht-network-firewall"
host_objects["nx-ht-snmp-network-router"]="nx-ht-network-router"
host_objects["nx-ht-snmp-network-switch"]="nx-ht-network-switch"

declare -A serviceset_objects
serviceset_objects["nx-ss-network-basic"]="nx-ss-network-snmp-basic"
serviceset_objects["nx-ss-network-extra"]="nx-ss-network-snmp-extra"
serviceset_objects["nx-ss-network-uptime"]="nx-ss-snmp-uptime"

echo "Removing legacy Director Objects"

## Rename command objects
for c in "${!command_objects[@]}"; do
    tmp=$(icingacli director command exist "$c")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director command set "$c" --object_name "${command_objects[$c]}"
    fi
done

## Rename host objects
for h in "${!host_objects[@]}"; do
    tmp=$(icingacli director host exist "$h")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director host set "$h" --object_name "${host_objects[$h]}"
    fi
done

## Rename service set objects
for s in "${!serviceset_objects[@]}"; do
    tmp=$(icingacli director serviceset exist "$s")
    if [[ $tmp != *"does not"* ]]; then
        icingacli director serviceset set "$s" --object_name "${serviceset_objects[$s]}"
    fi
done
