#!/bin/bash
## Add vendor file to import
. /usr/share/neteye/scripts/rpm-functions.sh
. /usr/share/neteye/secure_install/functions.sh

SERVICE="icingaweb2"
if is_cluster && ! is_active "$SERVICE" ; then
    echo "[i] Inactive Cluster Node. Skipping."
    exit 0
fi

FILE="/neteye/shared/icingaweb2/data/modules/fileshipper/nx-file-data/nx-vendor-list.csv"

if [ -f $FILE ]; then
    echo "File import '$FILE' already present... Nothing to do."
else
cat << EOF >> $FILE
"entry_name","entry_value","format","allowed_roles"
EOF

fi
