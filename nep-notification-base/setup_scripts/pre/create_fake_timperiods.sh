#!/bin/bash
## 

timeperiods=( "holidays" "" )

## Create timeperiod objects
for t in "${timeperiods}"; do
    tmp=$(icingacli director timeperiod exist "$t")
    if [[ $tmp == *"does not"* ]]; then
        icingacli director timeperiod create "$t" 
    fi
done
